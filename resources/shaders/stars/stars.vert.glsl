#version 330 core

uniform mat4 modelViewProjection;

in vec3 position;
in float starSize;
in float starLuminosity;

out float starLuminosityFragment;

void main()
{
	gl_Position = modelViewProjection * vec4(position, 1);
	gl_PointSize = starSize*30.0; // set star size with maximum of 30x30
	starLuminosityFragment = starLuminosity;
}
