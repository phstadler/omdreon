#version 330 core

out vec4 color;

in float starLuminosityFragment;

void main()
{
	// calculate distance to star center
	float centerDistance = length(gl_PointCoord - 0.5) * 2;
	if(centerDistance > 0.8)
	{
		// if distance is so high that the brightness is almost zero or below zero,
		// discard the fragment
		discard;
	}
	else
	{
		// Calculate fragment brightness from distance to star center
		float brightness = exp(1.0/(20.0*centerDistance))-1.05;
		brightness = clamp(brightness, 0.0, 1.0) * starLuminosityFragment;
		color = vec4(1,1,1,brightness);
	}
}