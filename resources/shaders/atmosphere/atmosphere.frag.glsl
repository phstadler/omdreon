#version 330 core

const float ambientIntensity = 0.05f;

uniform mat4 view;
uniform vec4 lightPositionWorld;

uniform mat4 modelView;
uniform mat3 normalMatrix;

in vec3 positionModelFragment;
in vec3 normalModelFragment;

vec3 positionEyeFragment;
vec3 normalEyeFragment;
vec3 positionModel;

out vec4 color;

void main() 
{

	positionModel = positionModelFragment - normalModelFragment;
	normalEyeFragment = normalMatrix * normalModelFragment;
	positionEyeFragment = (modelView * vec4(positionModel, 1)).xyz;

	//Directional vector from position to eye
	vec3 eyeDirPosition = normalize(-positionEyeFragment);

	//Directional vector from vertex Position to light position
	vec3 lightDirEye = normalize((view * vec4(vec3(lightPositionWorld), 1)).xyz - positionEyeFragment);

	//Angle between normal vector and position to eye vector
	float normalAngleEye = clamp(dot(eyeDirPosition, normalEyeFragment),0.0, 1.0);
	float invertedNormalAngleEye = 1.0-normalAngleEye;

	//Angle between normal vector and vertex position to eye vector
	float a = clamp(dot(normalEyeFragment, lightDirEye), 0.0, 1.0);
	float intensity = clamp(ambientIntensity + a, 0.0, 1.0);
	
	//functions to influence the opacity and color depending on the angle between normal vector and position to eye vector
	float curve1 = pow(invertedNormalAngleEye,4); 	//x^4
	float curve2 = cos(2* invertedNormalAngleEye);	//cos(2*x)

	//final calculation of the color and alpha value 
	//the base color of the atmosphere is a kind of blue -> vec3(0.1, 0.5, 1)
	//to get a brighter and less saturated color towards the planet the blue is mixed with white and
	//interpolated based on cos(2*x) where x is the inverted angle between normal and eye vector
	//the intensity is used to determin how bright the result is based on the angle the light hits it.
	color = vec4(mix(vec3(0.1, 0.5, 1), vec3(1, 1, 1), curve2) * intensity, curve2 * curve1*10*intensity);

}