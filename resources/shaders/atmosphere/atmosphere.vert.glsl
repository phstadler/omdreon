#version 330 core

in vec3 position;
in vec3 normal;
in vec2 texCoords;

out vec3 positionModelFragment;
out vec3 normalModelFragment;

uniform mat4 modelViewProjection;
uniform mat4 modelView;
uniform mat4 projection;
uniform mat3 normalMatrix;

void main() 
{
	positionModelFragment = position;
	normalModelFragment = normal;
	gl_Position  = modelViewProjection * vec4(position, 1);
}