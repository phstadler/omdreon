#version 330 core

#extension GL_ARB_shading_language_include : require
#include </noise3D.glsl>

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;
uniform sampler2D bottomDepth;
uniform vec2 windowSize;
uniform vec3 frontWorld;
uniform vec3 backWorld;
uniform vec4 lightPositionWorld;
uniform sampler2D shadowMap;
uniform mat4 shadowMatrix;
uniform mat3 normalMatrix;
// wave uniforms
uniform float time;

const float pi = 3.14159;

in float heightNormalizedFragment;
in vec3 positionScreenFragment;
in vec3 positionEyeFragment;
in vec3 positionWorldFragment;
in vec3 normalEyeFragment;
in vec3 normalModelFragment;
in vec2 texCoordsFragment;

out vec4 color;

const float ambientIntensity = 0.05f;
const vec3 oceanDark = vec3(0.2, 0.2, 0.35);
const vec3 oceanLight = vec3(0.5, 0.65, 0.8);
//const vec3 oceanLight = vec3(1, 1, 1);
const float coastMaxDepth = 0.4;
const float transparentHeight = 0.485;
const float reflectivity = 50;

float depthValue()
{
	mat4 viewProjection = projection*view;
	vec2 screenCoords = gl_FragCoord.xy / windowSize;

	float frontDepth = (viewProjection * vec4(frontWorld, 1.0)).z;
	float backDepth  = (viewProjection * vec4(backWorld,  1.0)).z;

	float depthNormalized = (positionScreenFragment.z - frontDepth) / (backDepth - frontDepth);
	float depthTexNormalized = texture(bottomDepth, screenCoords).r;

	float depthVal = (depthTexNormalized-depthNormalized);
	depthVal = clamp(depthVal+0.9, 0.0, 1.0);

	return  depthVal;
}

float turnVal(float frequency, float speed, vec2 texOffset)
{
	return sin((length(texCoordsFragment + texOffset)*2*3.1415 + time * speed)*frequency);
}

void main() 
{
	float uvX = texCoordsFragment.x;
	float uvY = texCoordsFragment.y;
	float turn1 = turnVal(1200, 0.05, vec2(0.1, -0.2));//sin(((uvX+uvY)*2*3.1415 + time/50);
	float turn2 = turnVal(130, 0.1, vec2(0.4, 0.05));//sin(((uvX)*2*3.1415 + time/2) *450);
	float turn3 = turnVal(630, 0.01, vec2(0.05, -0.7));
	float turn4 = turnVal(70, 0.17, vec2(-0.8, 0.45));

	turn1 = snoise(normalModelFragment*10 + vec3(time,0,-time)*1.5);
	turn2 = snoise(normalModelFragment*20 + vec3(0,0,time)*1.5)*0.2;
	turn3 = snoise(normalModelFragment*30 + vec3(time,time,-time)*1.5)*0.4;
	turn4 = snoise(normalModelFragment*40 + vec3(0,0,-time)*0.3)*0.1;
	
	vec3 right = normalize(cross(normalEyeFragment, vec3(0, 1, 0)));
	vec3 left = -right;
	vec3 up = normalize(cross(normalEyeFragment, right));
	vec3 down = -up;

	float mixAlpha1 = turn1 * 0.2 + 0.5;
	float mixAlpha2 = turn2 * 0.2 + 0.5;
	float mixAlpha3 = turn3 * 0.2 + 0.5;
	float mixAlpha4 = turn4 * 0.2 + 0.5;
	vec3 normalEyeFragment = normalize(mix((left+normalEyeFragment), (right+normalEyeFragment), mixAlpha1) +
									   mix((left+normalEyeFragment), (right+normalEyeFragment), mixAlpha2) +
									   mix((left+normalEyeFragment), (right+normalEyeFragment), mixAlpha3) +
	                                   mix((up+normalEyeFragment), (down+normalEyeFragment), mixAlpha4));

	
	//color = vec4(mixAlpha, mixAlpha, mixAlpha, 1);
	

    // volumetric ocean floor effect
	float depth = depthValue();

	vec3 lightPositionEye = (view * vec4(vec3(lightPositionWorld), 1)).xyz;
	vec3 lightDirEye = normalize(lightPositionEye - positionEyeFragment);
	vec3 H = normalize(lightDirEye - positionEyeFragment);

	
	// LAMBERT
	float diffuse = clamp(dot(normalEyeFragment, lightDirEye), 0.0, 1.0);
	
	// BLINN
	float spec = max(0, pow(dot(normalEyeFragment, H), reflectivity));

	float intensity = ambientIntensity + diffuse + spec;
	

	float coastFactor = min(1-depth, coastMaxDepth) / coastMaxDepth;

	float alpha = (heightNormalizedFragment > transparentHeight)
	                         ? 1-((heightNormalizedFragment - transparentHeight) / (0.5 - transparentHeight))
	                         : 0.95;

	color.rgb = mix(oceanDark, oceanLight, coastFactor) * intensity;
	//color.rgb += specularCol * spec *0.2 + diffuse;
	color.a   = alpha;

	// "FRESNEL"
	color.rgb *= diffuse;

	// shadows
	vec3 posShadow = (shadowMatrix * vec4(positionWorldFragment, 1)).xyz;
    vec2 UV = (posShadow.xy+vec2(1,1))/2.0;
    float pos = (posShadow.z+1)/2.0;
    float zMap = texture(shadowMap, UV).r;
    float bias = 0.005*tan(acos(diffuse));
    bias = clamp(bias, 0.0, 0.0001);
    if (zMap+bias <= pos) 
    {
    	color.rgb *= 0.1;
    }
    
}