#version 330 core

#extension GL_ARB_shading_language_include : require
#include </terrain.glsl>

in vec3 position;
in vec3 normal;
in vec2 texCoords;

out float heightNormalizedGeometry;
out vec3 positionModelGeometry;
out vec3 normalModelGeometry;
out vec2 texCoordsGeometry;

void main() 
{
	vec3 displacedPos;
	terrain(normal, position, displacedPos, heightNormalizedGeometry);

	positionModelGeometry = position;
	normalModelGeometry = normal;
	texCoordsGeometry = texCoords;
}