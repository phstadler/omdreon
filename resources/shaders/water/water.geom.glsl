#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out; // 3*1=3

uniform mat4 modelViewProjection;
uniform mat4 modelView;
uniform mat4 model;
uniform mat4 projection;
uniform mat3 normalMatrix;

in float heightNormalizedGeometry[];
in vec3 positionModelGeometry[];
in vec3 normalModelGeometry[];
in vec2 texCoordsGeometry[];

out float heightNormalizedFragment;
out vec3 positionScreenFragment;
out vec3 positionEyeFragment;
out vec3 normalEyeFragment;
out vec3 normalModelFragment;
out vec2 texCoordsFragment;
out vec3 positionWorldFragment;

const float shellDepth = 0.08;
const int numShells = 1;

void main() 
{
	/*
	for(int i = 0; i < 3; ++i)
	{
		gl_Position = modelViewProjection * vec4(positionModelGeometry[i], 1);
		positionEyeFragment = vec3(modelView * vec4(positionModelGeometry[i], 1));
		normalEyeFragment   = normalModelGeometry[i];
		EmitVertex();
	}
	EndPrimitive();
	*/

	// SHELLS

	for (int layerIdx = numShells-1; layerIdx >= 0; --layerIdx)
	{
		vec3 positionModel;
		for (int i = 0; i < 3; ++i)
		{
			heightNormalizedFragment = heightNormalizedGeometry[i];
			positionModel = positionModelGeometry[i] - normalModelGeometry[i] / numShells * shellDepth * layerIdx;
			normalEyeFragment = normalMatrix * normalModelGeometry[i];
			positionEyeFragment = (modelView * vec4(positionModel, 1)).xyz;
			positionWorldFragment = (model * vec4(positionModel, 1)).xyz;
			positionScreenFragment = (modelViewProjection * vec4(positionModel, 1)).xyz;
			normalModelFragment = normalModelGeometry[i];
			texCoordsFragment = texCoordsGeometry[i];
			gl_Position = projection * vec4(positionEyeFragment, 1);
			EmitVertex();
		}
		EndPrimitive();
	}
}