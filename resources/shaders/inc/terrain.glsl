#extension GL_ARB_shading_language_include : require
#include </noise3D.glsl>

uniform vec3 terrainSeed;

void terrain(vec3 normal, vec3 position, out vec3 displacedPosition, out float heightNormalized)
{
    const float displacementHeight = 0.2;
    const float detail1 = 0.9;
    const float weight1 = 2.0;
    const float detail2 = 5.1;
    const float weight2 = 0.5;
    const float detail3 = 5.1;
    const float weight3 = 0.5;
    const float heightCorrection = -0.15;

    float noise = (snoise(terrainSeed+normal*detail1)*weight1 +
                   snoise(terrainSeed+normal*detail2)*weight2 +
                   snoise(terrainSeed+normal*detail3)*weight3) / (weight1+weight2+weight3);

    noise = clamp(noise + heightCorrection, -1.0, 1.0);
    heightNormalized = (noise+1.0) / 2.0;
    displacedPosition = position + normal * noise * displacementHeight;
}