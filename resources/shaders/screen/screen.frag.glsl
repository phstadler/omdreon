#version 330 core

uniform sampler2D offscreenColor;
uniform sampler2D offscreenDepth;
uniform sampler2D shadowMap;

in vec2 texCoordsFragment;

out vec3 color;

void main()
{
	float depth = 1.0-texture(offscreenDepth, texCoordsFragment).r;
	color = vec3(depth, depth, depth);
	color = texture(shadowMap, texCoordsFragment).rgb;
}