#version 330 core

in vec3 position;
in vec2 texCoords;

out vec2 texCoordsFragment;

void main()
{
	// fullscreen quad position is in screen coordinates, just pass through
	gl_Position = vec4(position, 1);
	texCoordsFragment = texCoords;
}
