#version 330 core

uniform mat4 modelViewProjection;

in vec3 position;

out vec3 positionScreen;

void main()
{
	gl_Position = modelViewProjection * vec4(position, 1); 
	positionScreen = gl_Position.xyz;
}