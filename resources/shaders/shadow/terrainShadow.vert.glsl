#version 330 core

#extension GL_ARB_shading_language_include : require
#include </terrain.glsl>

uniform mat4 modelViewProjection;

in vec3 position;
in vec3 normal;

out vec3 positionScreen;

void main()
{
	vec3 displacedPositionModel;
	float heightNormalized;
	// depth should include terrain displacement, apply it
	terrain(normal, position, displacedPositionModel, heightNormalized);

	gl_Position = modelViewProjection * vec4(displacedPositionModel, 1); 
	positionScreen = gl_Position.xyz;
}