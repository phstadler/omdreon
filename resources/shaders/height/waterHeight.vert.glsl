#version 330 core

uniform mat4 modelViewProjection;
uniform mat4 model;

in vec3 position;

out vec3 positionScreen;

void main()
{
	gl_Position = modelViewProjection * vec4(position, 1);
	positionScreen = (modelViewProjection * vec4(position, 1)).xyz;
}
