#version 330 core

#extension GL_ARB_shading_language_include : require
#include </terrain.glsl>

uniform mat4 modelViewProjection;
uniform mat4 modelView;
uniform mat4 model;

in vec3 position;
in vec3 normal;

out float heightNormalized;
out vec3 positionEye;
out vec4 positionScreen;
out vec3 positionWorld;

void main()
{
    vec3 displacedPos;
    terrain(normal, position, displacedPos, heightNormalized);
	gl_Position = modelViewProjection * vec4(displacedPos, 1.0);
	positionScreen = gl_Position;
	positionEye = (modelView * vec4(position, 1)).xyz;
	positionWorld = (model * vec4(position, 1)).xyz;
}