#version 330 core

#extension GL_ARB_shading_language_include : require
#include </noise3D.glsl>

uniform mat4 view;
uniform mat4 projection;
uniform vec3 camPos;
uniform vec3 frontWorld;
uniform vec3 backWorld;

in vec3 positionScreen;

out vec4 color;

void main()
{
	const float variance = 0.05;

	mat4 viewProjection = projection*view;

	// Calculate nearest and farthest point on the planet in screen space for the depth map
	float frontDepth = (viewProjection * vec4(frontWorld, 1.0)).z;
	float backDepth  = (viewProjection * vec4(backWorld,  1.0)).z;

	// Calculate normal in range 0..1 for how far back the fragment is relative to the planet
	float depthTexNormalized = (positionScreen.z - frontDepth) / (backDepth - frontDepth);

	color = vec4(depthTexNormalized, depthTexNormalized, depthTexNormalized, 1);
	gl_FragDepth = depthTexNormalized;
}