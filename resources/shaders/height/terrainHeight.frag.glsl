#version 330 core

uniform mat4 view;
uniform mat4 projection;

uniform vec3 camPos;
uniform vec3 frontWorld;
uniform vec3 backWorld;

in float heightNormalized;
in vec3 positionEye;
in vec4 positionScreen;
in vec3 positionWorld;

out vec4 color;

void main()
{
	mat4 viewProjection = projection*view;

	// Calculate nearest and farthest point on the planet in screen space for the depth map
	float frontDepth = (viewProjection * vec4(frontWorld, 1.0)).z;
	float backDepth  = (viewProjection * vec4(backWorld,  1.0)).z;

	// Calculate depth in range 0..1 for how far back the fragment is relative to the planet
	float depthTexNormalized = (positionScreen.z - frontDepth) / (backDepth - frontDepth);

	// Write the depth to both the color attachment and the depth attachment
	color = vec4(depthTexNormalized, depthTexNormalized, depthTexNormalized, 1);
	gl_FragDepth = depthTexNormalized;
}