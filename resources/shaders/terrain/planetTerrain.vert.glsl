#version 330 core

#extension GL_ARB_shading_language_include : require
#include </terrain.glsl>

const float displacementHeight = 0.2;

uniform mat4 model;
uniform mat4 modelView;
uniform mat4 modelViewProjection;
uniform mat3 normalMatrix;

in vec3 position;
in vec3 normal;
in vec2 texCoords;

out float heightNormalizedGeometry;
out vec2 texCoordsGeometry;
out vec3 normalEyeGeometry;
out vec3 positionScreenGeometry;
out vec4 positionWorldGeometry;
out vec3 displacedPositionModelGeometry;


void main()
{
    // Convert light dir and normal to worldspace
    normalEyeGeometry  = normalize(normalMatrix * normal);
    texCoordsGeometry = texCoords;

    terrain(normal, position, displacedPositionModelGeometry, heightNormalizedGeometry);

    // Convert position to clip space and pass along
    gl_Position = modelViewProjection * vec4(displacedPositionModelGeometry, 1.0);
    positionScreenGeometry  = vec3(modelView * vec4(displacedPositionModelGeometry, 1));
    positionWorldGeometry = model * vec4(position, 1);
}