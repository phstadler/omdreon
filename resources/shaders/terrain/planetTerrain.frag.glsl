#version 330 core

const float ambientIntensity = 0.05f;

uniform mat4 view;

uniform vec4 lightPositionWorld;
uniform sampler2D terrainMix;
uniform sampler2D terrainMix2;
uniform sampler2D terrain0;
uniform sampler2D terrain1;
uniform sampler2D terrain2;
uniform sampler2D shadowMap;
uniform mat4 shadowMatrix;
uniform sampler2D terrain3;
uniform sampler2D terrain4;
uniform sampler2D coast1;

out vec4 color;

in float heightNormalized;
in vec2 texCoordsFragment;
in vec3 normalEyeFragment;
in vec3 positionScreenFragment;
in vec4 positionWorldFragment;

vec4 terrainColor()
{
	vec2 coords = mod(texCoordsFragment * 4, 1.0);

	vec4 terrain0Color = texture(terrain0, coords);
	vec4 terrain1Color = texture(terrain1, coords);
	vec4 terrain2Color = texture(terrain2, coords);
	vec4 terrain3Color = texture(terrain3, coords);
	vec4 terrain4Color = texture(terrain4, coords);
	vec4 coastColor = texture(coast1, coords);

	vec4 mix = texture(terrainMix, vec2(heightNormalized, texCoordsFragment.y));
	vec4 mix2 = texture(terrainMix2, vec2(heightNormalized, texCoordsFragment.y));


	vec4 color =  terrain0Color * mix.r +
	              terrain1Color * mix.g +
	              terrain2Color * mix.b +
	              terrain3Color * mix2.r / 2 +
	              terrain4Color * mix2.b +
	              clamp(coastColor * mix.r,0.0,0.4);
	

	color.a = 1;
	return color;
}

void main()
{
	vec3 lightdirEye  = normalize((view * vec4(vec3(lightPositionWorld), 1.0)).xyz - positionScreenFragment);

	// diffuse with lambert
    float diffuse = clamp(dot(lightdirEye, normalEyeFragment), 0.0, 1.0);
    float lightIntensity = clamp(ambientIntensity + diffuse, 0.0, 1.0);
    //lightIntensity = 1;
    color = terrainColor() * lightIntensity;

    color = vec4(color.rgb, 1);
    color.r += color.r * 0.3;


    // SHADOWS
    vec3 posShadow = (shadowMatrix * positionWorldFragment).xyz;
    vec2 UV = (posShadow.xy+vec2(1,1))/2.0;
    float pos = (posShadow.z+1)/2.0;
    float zMap = texture(shadowMap, UV).r;
    float bias = 0.005*tan(acos(diffuse));
    bias = clamp(bias, 0.0, 0.0001);
    if (zMap+bias <= pos) 
    {
    	color *= 0.1;
    }
}