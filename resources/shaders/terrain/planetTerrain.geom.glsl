#version 330 core

#extension GL_ARB_shading_language_include : require
#include </terrain.glsl>

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

uniform mat3 normalMatrix;

in vec3 normalEyeGeometry[];
in float heightNormalizedGeometry[];
in vec2 texCoordsGeometry[];
in vec3 displacedPositionModelGeometry[];
in vec3 positionScreenGeometry[];
in vec4 positionWorldGeometry[];

out float heightNormalized;
out vec2 texCoordsFragment;
out vec3 normalEyeFragment;
out vec3 positionScreenFragment;
out vec4 positionWorldFragment;

/**
 * Calculate normal with fake third vertex approximated by moving the first
 * position in the oppositve direction of the two other positions.
 *
 * This achieves something similar to calculating the normal with triangles
 * with adjacency information. Just not as good ):
 */
vec3 calculateNormal(vec3 pos, vec3 otherPos1, vec3 otherPos2)
{
	const float sphereRadius = 2.0;
	const float positionSampleDistance = 0.0005;

	float heightNormalized; // not used

	vec3 toOther1 = otherPos1-pos;
	vec3 toOther2 = otherPos2-pos;

	vec3 otherNormalUndisplaced3 = normalize(pos - normalize(toOther1+toOther2) * positionSampleDistance); 
	vec3 otherPosUndisplaced3 = otherNormalUndisplaced3 * sphereRadius;
	vec3 otherPos3;
	terrain(otherNormalUndisplaced3, otherPosUndisplaced3, otherPos3, heightNormalized);

	vec3 toOther3 = otherPos3-pos;

	return normalMatrix * normalize(cross(toOther1, toOther2) +
		                            cross(toOther2, toOther3) +
		                            cross(toOther1, toOther3));
}

void main()
{
	vec3 normals[3];
	normals[0] = mix(calculateNormal(displacedPositionModelGeometry[0], displacedPositionModelGeometry[1], displacedPositionModelGeometry[2]), normalEyeGeometry[0], 0.5);
	normals[1] = mix(calculateNormal(displacedPositionModelGeometry[1], displacedPositionModelGeometry[2], displacedPositionModelGeometry[0]), normalEyeGeometry[1], 0.5);
	normals[2] = mix(calculateNormal(displacedPositionModelGeometry[2], displacedPositionModelGeometry[0], displacedPositionModelGeometry[1]), normalEyeGeometry[2], 0.5);

	for(int i = 0; i < 3; i++)
	{
		gl_Position = gl_in[i].gl_Position;
		heightNormalized = heightNormalizedGeometry[i];
		texCoordsFragment = texCoordsGeometry[i];
		positionScreenFragment = positionScreenGeometry[i];
		normalEyeFragment = normals[i];
		positionWorldFragment = positionWorldGeometry[i];
		EmitVertex();
	}
	EndPrimitive();
}