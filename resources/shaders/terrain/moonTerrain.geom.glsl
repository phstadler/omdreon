#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec3 normalEyeGeometry[];
in vec2 texCoordsGeometry[];
in vec3 positionScreenGeometry[];
in vec4 positionWorldGeometry[];

out vec2 texCoordsFragment;
out vec3 normalEyeFragment;
out vec3 positionScreenFragment;
out vec4 positionWorldFragment;

void main()
{
	// TODO Calculate normals, this passes through at the moment but normals change when a displacement is applied

	/*vec3 dir1 = vec3(gl_in[1].gl_Position - gl_in[0].gl_Position);
	vec3 dir2 = vec3(gl_in[2].gl_Position - gl_in[0].gl_Position);
	vec3 normal = normalize(cross(dir1, dir2));*/

	for(int i = 0; i < 3; i++)
	{
		gl_Position = gl_in[i].gl_Position;
		texCoordsFragment = texCoordsGeometry[i];
		positionScreenFragment = positionScreenGeometry[i];
		normalEyeFragment = normalEyeGeometry[i];
		positionWorldFragment = positionWorldGeometry[i];
		EmitVertex();
	}
	EndPrimitive();
}