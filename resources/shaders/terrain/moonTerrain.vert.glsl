#version 330 core

const float displacementHeight = 0.5;

uniform sampler2D terrain0;
uniform mat4 model;
uniform mat4 modelView;
uniform mat4 modelViewProjection;
uniform mat3 normalMatrix;

in vec3 position;
in vec3 normal;
in vec2 texCoords;

out vec2 texCoordsGeometry;
out vec3 normalEyeGeometry;
out vec3 positionScreenGeometry;
out vec4 positionWorldGeometry;


void main()
{
    // Convert light dir and normal to worldspace
    normalEyeGeometry  = normalize(normalMatrix * normal);
    texCoordsGeometry = texCoords;

    vec4 terrain0Disp = texture(terrain0, texCoords);

    vec3 displacedPositionModel = position + normal * terrain0Disp.xyz * displacementHeight;

    // Convert position to clip space and pass along
    gl_Position = modelViewProjection * vec4(displacedPositionModel, 1.0);
    positionScreenGeometry  = vec3(modelView * vec4(displacedPositionModel, 1));
    positionWorldGeometry = model * vec4(position, 1);
}