#version 330 core

const float ambientIntensity = 0.05f;

uniform mat4 view;
uniform mat3 normalMatrix;

uniform vec4 lightPositionWorld;
uniform sampler2D terrain0;
uniform sampler2D terrainNormal;
uniform sampler2D shadowMap;
uniform mat4 shadowMatrix;

out vec4 color;

in vec2 texCoordsFragment;
in vec3 normalEyeFragment;
in vec3 positionScreenFragment;
in vec4 positionWorldFragment;

const float textureSize = 1024.0;
const float texelSize = 1.0 / 1024.0;

vec4 terrainColor()
{
	vec2 coords = texCoordsFragment; //mod(texCoordsFragment * 4, 1.0);

	vec4 terrain0Color = texture(terrain0, coords);
    

	vec4 color =  terrain0Color;
	

	color.a = 1;
	return color;
}

void main()
{
	vec3 lightdirEye  = normalize((view * vec4(vec3(lightPositionWorld), 1.0)).xyz - positionScreenFragment);


    vec4 encodedNormal = texture(terrainNormal, texCoordsFragment);
    vec3 localCoords = 2.0 * encodedNormal.rgb - vec3(1.0,1.0,1.0);
    vec3 normalDirection = normalMatrix * normalize(localCoords); //normalize(positionWorldFragment.xyz * localCoords);

    normalDirection = mix(normalDirection, normalEyeFragment, 0.9);

	// diffuse with lambert
    float diffuse = clamp(dot(lightdirEye, normalDirection), 0.0, 1.0);
    float lightIntensity = clamp(ambientIntensity + diffuse, 0.0, 1.0);
    //lightIntensity = 1;
    color = terrainColor() * lightIntensity;

    color = vec4(color.rgb, 1);
    //color.rg = texCoordsFragment;
    //color.ba = vec2(0,1);


    // SHADOWS
    vec3 posShadow = (shadowMatrix * positionWorldFragment).xyz;
    vec2 UV = (posShadow.xy+vec2(1,1))/2.0;
    float pos = (posShadow.z+1)/2.0;
    float zMap = texture(shadowMap, UV).r;
    float bias = 0.005*tan(acos(diffuse));
    bias = clamp(bias, 0.0, 0.0001);
    if (zMap+bias <= pos) 
    {
    	color *= 0.1;
    }

}

