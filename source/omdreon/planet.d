module omdreon.planet;

private
{
	import std.stdio;
	import std.random;
	import std.range;
	import callisto.renderer;
	import callisto.primitives;
	import callisto.navigation;
	import callisto.actor;
	import callisto.error;
}

class Planet
{
    this(Shell shell, Camera mainCamera)
    {
        this.shell = shell;

        planet = new SceneNode;

        depthTarget = new RenderTarget(shell.window.framebufferSize);
        shadowTarget = new RenderTarget(shell.window.framebufferSize);

        vec2i shadowTexSize = shell.window.framebufferSize;
        shadowTexSize.x *= 80;
        shadowTexSize.y *= 3;
        shadowCam = new OrthographicCam(shadowTexSize, 10.0);
        shadowCam.nearPlane = 0.01f;
        shadowCam.farPlane = 1000f;

        cam = mainCamera;

        initGL();
        initTerrain();
        initWater();
        initAtmosphere();
        initShadowRendering();
        initScreenRendering();
        initTerrainHeightRenderer();
        intiWaterHeightRenderer();
        initMoon();
        setRandomTerrainSeed();

        auto navigation = new OrbitalNavigation(shell, cam);

        shell.updateQueue ~= &advanceTime;
        shell.updateQueue ~= &rotate;
        shell.updateQueue ~= { navigation.center = planet.position; };
        shell.updateQueue ~= {
            backWorld = planet.position;
            frontWorld = planet.position - cam.forward * size_ * 1.1;
            windowSize = vec2(Shell.mainShell.window.framebufferSize);
        };
        shell.keyEventQueue ~= (KeyEvent event) {
            if(event.press) setRandomTerrainSeed();
        };
    }

    void setRandomTerrainSeed()
    {
        enum seedRange = 1_000_000;
        vec3 seed = vec3(uniform(-seedRange, seedRange),
                         uniform(-seedRange, seedRange),
                         uniform(-seedRange, seedRange));

        terrainHeightRenderer.setUniform("terrainSeed", seed);
        terrainRenderer.setUniform("terrainSeed", seed);
        waterRenderer.setUniform("terrainSeed", seed);
        waterHeightRenderer.setUniform("terrainSeed", seed);
        atmosphereRenderer.setUniform("terrainSeed", seed);
        screenRenderer.setUniform("terrainSeed", seed);
        shadowRenderer.setUniform("terrainSeed", seed);
        terrainShadowRenderer.setUniform("terrainSeed", seed);
        moonRenderer.setUniform("terrainSeed", seed);
    }

    void renderDepth(float lag)
    {

        updateShadowCam(); // updates the shadowCam matrices
        // render all objects with shadowRenderer;
        glCullFace(GL_FRONT);
        terrainShadowRenderer.render(terrain);
        terrainShadowRenderer.render(moons.children);
        shadowRenderer.render(water);
        glCullFace(GL_BACK);

        terrainHeightRenderer.setUniform("backWorld", backWorld);
        terrainHeightRenderer.setUniform("frontWorld", frontWorld);
        terrainHeightRenderer.setUniform("camPos", cam.position);
        terrainHeightRenderer.render(terrain);

        waterHeightRenderer.setUniform("backWorld", backWorld);
        waterHeightRenderer.setUniform("frontWorld", frontWorld);
        waterHeightRenderer.setUniform("camPos", cam.position);

        glFrontFace(GL_CW);
        waterHeightRenderer.render(water);
        glFrontFace(GL_CCW);
    }

    void renderPlanetAndMoon(float lag)
    {
        mat4 shadowMVP = shadowCam.projection * shadowCam.view;
        terrainRenderer.setUniform("shadowMatrix", shadowMVP);
        terrainRenderer.render(terrain);

        waterRenderer.setUniform("time", time);
        waterRenderer.setUniform("windowSize", windowSize);
        waterRenderer.setUniform("backWorld", backWorld);
        waterRenderer.setUniform("frontWorld", frontWorld);
        waterRenderer.setUniform("shadowMatrix", shadowMVP);
        waterRenderer.setUniform("camPos", cam.position);
        waterRenderer.render(water);

        moonRenderer.setUniform("shadowMatrix", shadowMVP);
        moonRenderer.render(moons.children);

        //screenRenderer.render(fullscreenQuad);
    }

    void renderAtmosphere(float lag)
    {
        atmosphereRenderer.render(atmosphere);
    }

    void initGL()
    {
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_MULTISAMPLE);
    }

    void initScreenRendering()
    {
        screenRenderer = new Renderer(
            shell,
            textResource("shaders/screen/screen.vert.glsl"),
            textResource("shaders/screen/screen.frag.glsl"),
            null,
            null,
            cam
        );
        screenRenderer.setTexture("offscreenColor", depthTarget.colorTex);
        screenRenderer.setTexture("offscreenDepth", depthTarget.depthTex);
        screenRenderer.setTexture("shadowMap", shadowTarget.colorTex);

        fullscreenQuad = Actor(quad(2.0f, 2.0f));
    }

    void initShadowRendering()
    {
        shadowRenderer = new Renderer(
            shell,
            textResource("shaders/shadow/shadow.vert.glsl"),
            textResource("shaders/shadow/shadow.frag.glsl"),
            null,
            null,
            shadowCam
        );
        shadowRenderer.renderTarget = shadowTarget;

        terrainShadowRenderer = new Renderer(
            shell,
            textResource("shaders/shadow/terrainShadow.vert.glsl"),
            textResource("shaders/shadow/terrainShadow.frag.glsl"),
            null,
            [
                "/noise2D.glsl": textResource("shaders/inc/noise2D.glsl"),
                "/noise3D.glsl": textResource("shaders/inc/noise3D.glsl"),
                "/noise4D.glsl": textResource("shaders/inc/noise4D.glsl"),
                "/terrain.glsl": textResource("shaders/inc/terrain.glsl")
            ],
            shadowCam
        );
        terrainShadowRenderer.renderTarget = shadowTarget;
    }

    void updateShadowCam()
    {

        vec3 fwd = (terrain.worldPosition - shadowCam.position).normalized;
        vec3 right = cross(fwd, vec3(0, 1, 0)).normalized;
        vec3 up = cross(fwd, right).normalized;

        shadowCam.set(lightPositionWorld.xyz, -fwd, up);
    }

    void initTerrainHeightRenderer()
    {
        terrainHeightRenderer = new Renderer(
            shell,
            textResource("shaders/height/terrainHeight.vert.glsl"),
            textResource("shaders/height/terrainHeight.frag.glsl"),
            null,
            null,
            cam
        );
        terrainHeightRenderer.renderTarget = depthTarget;
    }

    void intiWaterHeightRenderer()
    {
        waterHeightRenderer = new Renderer(
            shell,
            textResource("shaders/height/waterHeight.vert.glsl"),
            textResource("shaders/height/waterHeight.frag.glsl"),
            null,
            null,
            cam
        );
        waterHeightRenderer.renderTarget = depthTarget;
    }

    void initTerrain()
    {
        initTerrainActor();
        initTerrainRenderer();
    }

    void initTerrainActor()
    {
        terrain = new SceneNode;
        terrain.mesh = icosphere(size_, tesselation);
        planet.addChild(terrain);
    }

    void initTerrainRenderer()
    {
        terrainRenderer = new Renderer(
            shell,
            textResource("shaders/terrain/planetTerrain.vert.glsl"),
            textResource("shaders/terrain/planetTerrain.frag.glsl"),
            textResource("shaders/terrain/planetTerrain.geom.glsl"),
            [
                "/noise2D.glsl": textResource("shaders/inc/noise2D.glsl"),
                "/noise3D.glsl": textResource("shaders/inc/noise3D.glsl"),
                "/noise4D.glsl": textResource("shaders/inc/noise4D.glsl"),
                "/terrain.glsl": textResource("shaders/inc/terrain.glsl")
            ],
            cam
        );
        terrainRenderer.setTexture("terrainMix",         dataResource("images/terrain_mix.png"));
        terrainRenderer.setTexture("terrainMix2",        dataResource("images/terrain_mix2.png"));
        terrainRenderer.setTexture("terrain0",           dataResource("images/desert.png"));
        terrainRenderer.setTexture("terrain1",           dataResource("images/desertRock.png"));
        terrainRenderer.setTexture("terrain2",           dataResource("images/mountainTop.png"));
        terrainRenderer.setTexture("terrain3",           dataResource("images/green.png"));
        terrainRenderer.setTexture("terrain4",           dataResource("images/snow.png"));
        terrainRenderer.setTexture("coast1",             dataResource("images/coast.png"));
        terrainRenderer.setTexture("shadowMap",          shadowTarget.depthTex);
        terrainRenderer.setUniform("lightPositionWorld", lightPositionWorld);
    }

    void initWater()
    {
        initWaterActor();
        initWaterRenderer();
    }

    void initWaterActor()
    {
        water = new SceneNode;
        water.mesh = icosphere(size_, tesselation);
        planet.addChild(water);
    }

    void initWaterRenderer()
    {
        waterRenderer = new Renderer(
            shell,
            textResource("shaders/water/water.vert.glsl"),
            textResource("shaders/water/water.frag.glsl"),
            textResource("shaders/water/water.geom.glsl"),
            [
                "/noise2D.glsl": textResource("shaders/inc/noise2D.glsl"),
                "/noise3D.glsl": textResource("shaders/inc/noise3D.glsl"),
                "/noise4D.glsl": textResource("shaders/inc/noise4D.glsl")
            ],
            cam
        );
        waterRenderer.setTexture("bottomDepth", depthTarget.depthTex);
        waterRenderer.setTexture("shadowMap",   shadowTarget.depthTex);
        waterRenderer.setUniform("lightPositionWorld", lightPositionWorld);
        waterRenderer.blendMode = BlendMode.multiplicative;
    }

    void initAtmosphere()
    {
        initAtmosphereActor();
        initAtmosphereRenderer();
    }

    void initAtmosphereActor()
    {
        atmosphere = new SceneNode;
        atmosphere.mesh = icosphere(size_+.2f, tesselation);
        planet.addChild(atmosphere);
    }

    void initAtmosphereRenderer()
    {
        atmosphereRenderer = new Renderer(
            shell,
            textResource("shaders/atmosphere/atmosphere.vert.glsl"),
            textResource("shaders/atmosphere/atmosphere.frag.glsl"),
            null,
            null,
            cam
        );
        atmosphereRenderer.blendMode = BlendMode.additive;
		atmosphereRenderer.setUniform("lightPositionWorld", lightPositionWorld);
    }

    void initMoon()
    {
        initMoonRenderer();

        Mesh moonMesh = icosphere(size_, tesselation / 2);

        moons = new SceneNode;

        enum moonCount = 2;
        foreach(i; 0..moonCount)
        {
            SceneNode moon = new SceneNode;

            float scale = uniform(moonSizeMin, moonSizeMax);
            moon.mesh = moonMesh;
            moon.position = vec3(30+i*20, 0, 0);
            moon.position = quat.axis_rotation(uniform(0.0, 2*PI), vec3(0,1,0)) * moon.position;
            moon.scale = vec3(scale, scale, scale);
            moons.addChild(moon);

            real orbitRotationSpeed = uniform(moonOrbitRotationSpeedMin, moonOrbitRotationSpeedMax);
            moonOrbitRotations ~= quat.axis_rotation(orbitRotationSpeed, vec3(0,-1,0));
        }
        planet.addChild(moons);
    }

    void initMoonRenderer()
    {
        moonRenderer = new Renderer(
            shell,
            textResource("shaders/terrain/moonTerrain.vert.glsl"),
            textResource("shaders/terrain/moonTerrain.frag.glsl"),
            textResource("shaders/terrain/moonTerrain.geom.glsl"),
            [
                "/noise2D.glsl": textResource("shaders/inc/noise2D.glsl"),
                "/noise3D.glsl": textResource("shaders/inc/noise3D.glsl"),
                "/noise4D.glsl": textResource("shaders/inc/noise4D.glsl"),
                "/terrain.glsl": textResource("shaders/inc/terrain.glsl")
            ],
            cam
        );
        moonRenderer.setTexture("terrain0",   dataResource("images/moonfull.png"));
        moonRenderer.setTexture("terrainNormal", dataResource("images/normal.png"));
        moonRenderer.setTexture("shadowMap", shadowTarget.depthTex);
        moonRenderer.setUniform("lightPositionWorld", lightPositionWorld);
    }

private:
    enum tesselation = 28;
    enum lightPositionWorld = vec4(100.8, 0.05, 100, 1.0);

    Shell shell;

    Camera cam;
    Camera shadowCam;

    SceneNode planet;
    SceneNode terrain;
    SceneNode water;
    SceneNode atmosphere;
    SceneNode moons;

    Actor fullscreenQuad;

    RenderTarget depthTarget;
    RenderTarget shadowTarget;

    Renderer terrainHeightRenderer;
    Renderer terrainRenderer;
    Renderer waterRenderer;
    Renderer waterHeightRenderer;
    Renderer atmosphereRenderer;
    Renderer screenRenderer;
    Renderer shadowRenderer;
    Renderer terrainShadowRenderer;
    Renderer moonRenderer;

    float size_ = 2f;

    enum float planetRotationSpeed = 0.0025;
    real planetZRotation = 0.0f;

    float time = 0;
    vec3 backWorld;
    vec3 frontWorld;
    vec2 windowSize;
    // Rotation of moon position per frame
    quat[] moonOrbitRotations;
    enum moonOrbitRotationSpeedMin = 0.0004;
    enum moonOrbitRotationSpeedMax = 0.01;
    enum moonSizeMin = 0.2;
    enum moonSizeMax = 0.5;

    void rotate()
    {
        planetZRotation += planetRotationSpeed;
        quat orientation = quat.axis_rotation(planetZRotation, vec3(0,1,0));
        planet.orientation = orientation;

        foreach(moon, planetZRotation; zip(moons.children, moonOrbitRotations))
        {
            moon.position = planetZRotation * moon.position;
        }
    }

    void advanceTime()
    {
        time += shell.timestep;
    }
}
