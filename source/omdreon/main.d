module omdreon.main;

import callisto.shell;
import callisto.camera;
import omdreon.planet;
import omdreon.stars;

void main()
{
    Shell shell = Shell.mainShell;
    shell.window.title = "Omdreon";

    Camera mainCamera = new PerspectiveCam(shell.window.framebufferSize);
    mainCamera.nearPlane = 0.1f;
    mainCamera.farPlane = 10000f;

    Planet planet = new Planet(shell, mainCamera);
    Stars stars = new Stars(shell, mainCamera);

    shell.renderQueue ~= &planet.renderDepth;
    shell.renderQueue ~= &planet.renderPlanetAndMoon;
    shell.renderQueue ~= &stars.render;
    shell.renderQueue ~= &planet.renderAtmosphere;

    shell.resizeEventQueue ~= (ResizeEvent e) {
        mainCamera.resize(e.size);
    };

    shell.run();
}
