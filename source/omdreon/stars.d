module omdreon.stars;

public
{
    import callisto.shell;
}

private
{
    import std.random;
    import callisto.mesh;
    import callisto.renderer;
    import callisto.primitives : positionDefault;
}

class Stars
{
    this(Shell shell, Camera cam)
    {
        this.cam = cam;
        initStarMesh();
        initStarRenderer(shell);
    }

    /++ Returns a random vector on the unit sphere +/
    vec3 randomStarPosition()
    {
        vec3 position =  vec3(uniform(-1.0, 1.0),
                              uniform(-1.0, 1.0) * uniform(-1.0, 1.0),
                              uniform(-1.0, 1.0)).normalized;

        position = quat.axis_rotation(PI*0.271f, vec3(1, 0, 0)) * position;
        position *= uniform(minStarDistance, maxStarDistance);
        return position;
    }

    void initStarRenderer(Shell shell)
    {
        starRenderer = new Renderer(
            shell,
            textResource("shaders/stars/stars.vert.glsl"),
            textResource("shaders/stars/stars.frag.glsl"),
            null,
            null,
            cam
        );
        starRenderer.blendMode = BlendMode.additive;
    }

    void initStarMesh()
    {
        GLfloat[] vertices;
        foreach(starIdx; 0..starCount)
        {
            vertices ~= randomStarPosition().vector;
            vertices ~= uniform(0.0, 1.0);
            vertices ~= uniform(minLuminosity, maxLuminosity) * uniform(0.0, 1.0);
        }
        starMesh = new Mesh(vertices, null, GL_POINTS, positionDefault,
                                                       MeshAttr("starSize", 1),
                                                       MeshAttr("starLuminosity", 1));
    }

    void render(float lag)
    {
        glDepthMask(GL_FALSE);
        glEnable(GL_PROGRAM_POINT_SIZE);
        starRenderer.render(starMesh);
        glDepthMask(GL_TRUE);
        glDisable(GL_PROGRAM_POINT_SIZE);
    }


private:
    Camera cam;
    Mesh starMesh;
    Renderer starRenderer;
    enum starCount = 100000;
    enum minStarDistance = 200f;
    enum maxStarDistance = 600f;
    enum minLuminosity = 0.4;
    enum maxLuminosity = 1.0;
}
