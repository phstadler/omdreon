module callisto.texture;

private
{
    import std.string : toStringz;
    import derelict.opengl3.gl3;
    import callisto.error;
    import gl3n.linalg;
}

struct Texture
{
    enum defaultTexParameters = [
        GL_TEXTURE_WRAP_S: GL_REPEAT,
        GL_TEXTURE_WRAP_T: GL_REPEAT,
        GL_TEXTURE_MIN_FILTER: GL_LINEAR,
        GL_TEXTURE_MAG_FILTER: GL_LINEAR
    ];

    static Texture fromData(ubyte[] data, GLsizei width, GLsizei height, GLenum target=GL_TEXTURE_2D, GLenum[GLenum] texParameters=defaultTexParameters)
    {
        return fromData(data.ptr, width, height, target, texParameters);
    }

    static Texture fromData(void* data, GLsizei width, GLsizei height, GLenum target=GL_TEXTURE_2D, GLenum[GLenum] texParameters=defaultTexParameters)
    {
        Texture texture = Texture.empty(target, texParameters);

        glTexImage2D(
            target,
            0,
            GL_RGBA8,
            width,
            height,
            0,
            GL_BGRA,
            GL_UNSIGNED_BYTE,
            data
        );

        return texture;
    }

    static Texture blank(vec2i size, GLenum target=GL_TEXTURE_2D, GLenum format=GL_RGB, GLenum internalFormat=GL_RGB, GLenum[GLenum] texParameters=defaultTexParameters)
    {
        Texture texture = Texture.empty(target, texParameters);

        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     format,
                     size.x,
                     size.y,
                     0,
                     internalFormat,
                     GL_UNSIGNED_BYTE,
                     null);

        return texture;
    }

    static Texture empty(GLenum target=GL_TEXTURE_2D, GLenum[GLenum] texParameters=defaultTexParameters)
    {
        Texture texture;

        glGenTextures(1, &texture.name);
        glBindTexture(target, texture.name);
        glActiveTexture(GL_TEXTURE0);

        foreach(key, value; texParameters)
        {
            glTexParameteri(target, key, value);
        }

        return texture;
    }

    GLuint name;

    void dispose()
    {
        glDeleteTextures(1, &name);
    }

    void use(int idx, GLenum type=GL_TEXTURE_2D)
    out
    {
        checkErrorsOpenGL();
    }
    body
    {
        glActiveTexture(GL_TEXTURE0 + idx);
        glBindTexture(type, name);
    }
}