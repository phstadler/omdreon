module callisto.img;

public
{
    import callisto.texture;
    import callisto.source;
}

private
{
    import derelict.freeimage.freeimage;
    import callisto.paths;
    import std.stdio;
}

static this()
{
    import derelict.util.exception;

    ShouldThrow missingSymFunc(string symName) {
        enum requiredSymbols = [
            "FreeImage_OpenMemory",
            "FreeImage_CloseMemory",
            "FreeImage_GetFileTypeFromMemory",
            "FreeImage_LoadFromMemory",
            "FreeImage_Unload",
            "FreeImage_ConvertTo32Bits",
            "FreeImage_GetWidth",
            "FreeImage_GetHeight",
            "FreeImage_GetPitch",
            "FreeImage_ConvertToRawBits"
        ];

        return requiredSymbols.canFind(symName)
                    ? ShouldThrow.Yes
                    : ShouldThrow.No;
    }


    DerelictFI.missingSymbolCallback = &missingSymFunc;
    version(OSX) DerelictFI.load(libraryPath("libfreeimage.3.16.0.dylib"));
    else version(Win32) DerelictFI.load(libraryPath("FreeImage.dll"));
    else DerelictFI.load();
}

/++
 + Load the PNG data in the given datasource into a texture
 +/
Texture loadTexture(DataSource source)
{
    ubyte[] data = cast(ubyte[]) source.read();

    auto stream = FreeImage_OpenMemory(data.ptr, cast(DWORD) data.length);
    scope(exit) FreeImage_CloseMemory(stream);

    auto format = FreeImage_GetFileTypeFromMemory(stream, 0);
    auto image = FreeImage_LoadFromMemory(format, stream);
    scope(exit) FreeImage_Unload(image);

    auto image32 = FreeImage_ConvertTo32Bits(image);
    scope(exit) FreeImage_Unload(image32);
    int width = FreeImage_GetWidth(image32);
    int height = FreeImage_GetHeight(image32);

    int scanWidth = FreeImage_GetPitch(image32);
    ubyte[] bits = new ubyte[scanWidth*height];
    FreeImage_ConvertToRawBits(bits.ptr, image32, scanWidth, 32, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);

    return Texture.fromData(bits, width, height);
}
