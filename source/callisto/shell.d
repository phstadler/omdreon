module callisto.shell;

public {
    import derelict.opengl3.gl3;
    import gl3n.linalg;
}

private {
    import callisto.glfw;
    import std.algorithm;
    import std.string;
    import std.datetime;
    import std.math;
    import std.traits;
    import std.array;
    import std.range;
}

alias Window!Shell ShellWindow;

/++
 + Provides a main loop, a window to draw in and event handling queues.
 +
 + Examples:
 + -------------------------------
 + Shell shell = Shell.mainShell;
 + shell.updateQueue ~= { writeln("Each frame"); };
 + shell.run();
 + -------------------------------
 +/
class Shell
{
    enum defaultTimestep = 1.0f/60.0f;
    /++
     + Returns the main shell with a timestep of 60UPS.
     +/
    static @property Shell mainShell() {
        if(mainShell_ is null) {
            mainShell_ = new Shell(defaultTimestep);
        }
        return mainShell_;
    }

    /++
     + Initializes a new shell with the given timestep.
     + The timestep specifies the time difference between
     + two update calls, render will be called as often as
     + possible.
     +/
    this(float timestep)
    {
        window_   = ShellWindow.newWindow;
        window_.userPointer = this;
        timestep_ = timestep;

        window_.keyCallback = cast(GLFWkeyfun) &glfwCallback!KeyEvent;
        window_.mouseMoveCallback = cast(GLFWcursorposfun) &glfwCallback!MouseMoveEvent;
        window_.mouseButtonCallback = cast(GLFWmousebuttonfun) &glfwCallback!MouseButtonEvent;
        window_.scrollCallback = cast(GLFWscrollfun) &glfwCallback!ScrollEvent;

        window_.resizeCallback = cast(GLFWframebuffersizefun) &glfwCallback!ResizeEvent;
        window_.focusCallback = cast(GLFWwindowfocusfun) &glfwCallback!FocusEvent;
    }

    /++
     + Enters the main loop, constantly updating and dispatching events
     + until the window is closed by the user.
     +/
	void run()
    {
        StopWatch stopWatch;
        TickDuration currentFrameTime, lastFrameTime;
        float nextUpdateTimeSec = timestep_;
        float deltaTime;
        bool shouldClose;
        int result = 0;

        stopWatch.start();
        lastFrameTime = stopWatch.peek();

        while(!window_.shouldClose)
        {
            currentFrameTime = stopWatch.peek();
            deltaTime = (lastFrameTime-currentFrameTime).to!("seconds", float);
            lastFrameTime = currentFrameTime;

            float currentFrameTimeSec = currentFrameTime.to!("seconds", float);
            if(nextUpdateTimeSec < currentFrameTimeSec) {
                nextUpdateTimeSec += timestep_;
                window_.pollEvents();
                updateQueue_();

                renderQueue_(deltaTime); // delta time for now, should be normalized lag 
                window_.swapBuffers();
            }
        }
    }

    /++ Clears all callbacks in the queues +/
    void clearQueues()
    {
        updateQueue_.clear();
        renderQueue_.clear();
        keyEventQueue_.clear();
        mouseButtonEventQueue_.clear();
        mouseMoveEventQueue_.clear();
        resizeEventQueue_.clear();
        scrollEventQueue_.clear();
        focusEventQueue_.clear();
    }

    @property float timestep() { return timestep_; }

    @property ShellWindow window() { return window_; }

    @property ref UpdateQueue updateQueue() { return updateQueue_; }
    @property ref RenderQueue renderQueue() { return renderQueue_; }
    @property ref KeyEventQueue keyEventQueue() { return keyEventQueue_; }
    @property ref MouseButtonEventQueue mouseButtonEventQueue() { return mouseButtonEventQueue_; }
    @property ref MouseMoveEventQueue mouseMoveEventQueue() { return mouseMoveEventQueue_; }
    @property ref ResizeEventQueue resizeEventQueue() { return resizeEventQueue_; }
    @property ref ScrollEventQueue scrollEventQueue() { return scrollEventQueue_; }
    @property ref FocusEventQueue focusEventQueue() { return focusEventQueue_; }

private:
    static Shell mainShell_;

    ShellWindow window_;
    float timestep_;

    UpdateQueue updateQueue_;
    RenderQueue renderQueue_;
    KeyEventQueue keyEventQueue_;
    MouseButtonEventQueue mouseButtonEventQueue_;
    MouseMoveEventQueue mouseMoveEventQueue_;
    ResizeEventQueue resizeEventQueue_;
    ScrollEventQueue scrollEventQueue_;
    FocusEventQueue focusEventQueue_;
}

// TODO how about template mixin
// mixin CallbackQueue!("mouseMoveEventQueue", MouseMoveEvent);
// Generates getters and setters for a callback
// Or just make them public?

struct CallbackQueue(T...)
{
    void delegate(T)[] callbacks;

    ref CallbackQueue!T opOpAssign(string op)(void delegate(T) callback) if(op == "~")
    {
        callbacks ~= callback;
        return this;
    }

    void opCall(T arguments)
    {
        foreach(callback; callbacks)
        {
            callback(arguments);
        }
    }

    void clear()
    {
        callbacks.length = 0;
    }
}

alias CallbackQueue!()                 UpdateQueue;
alias CallbackQueue!(float)            RenderQueue;
alias CallbackQueue!(KeyEvent)         KeyEventQueue;
alias CallbackQueue!(MouseButtonEvent) MouseButtonEventQueue;
alias CallbackQueue!(MouseMoveEvent)   MouseMoveEventQueue;
alias CallbackQueue!(ResizeEvent)      ResizeEventQueue;
alias CallbackQueue!(ScrollEvent)      ScrollEventQueue;
alias CallbackQueue!(FocusEvent)       FocusEventQueue;

struct KeyEvent
{
    int key; /// The keyboard key that was pressed or released
    int scancode; /// The system-specific scancode of the key
    int action; /// GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT
    int mods; /// Bit field describing which modifier keys were held down, e.g. GLFW_MOD_SHIFT

    @property bool press() { return action == GLFW_PRESS; }
    @property bool release() { return action == GLFW_RELEASE; }
    @property bool repeat() { return action == GLFW_REPEAT; }
}

struct MouseMoveEvent
{
    double x;
    double y;

    @property vec2 position() { return vec2(x, y); }
}

struct MouseButtonEvent
{
    int button; /// The mouse button that was pressed or released. e.g. GLFW_MOUSE_BUTTON_LEFT
    int action; /// GLFW_PRESS or GLFW_RELEASE
    int mods; /// Bit field describing which modifier keys were held down on the keyboard, e.g. GLFW_MOD_SHIFT.

    @property bool press() { return action == GLFW_PRESS; }
    @property bool release() { return action == GLFW_RELEASE; }
}

struct ResizeEvent
{
    int width;
    int height;

    @property vec2i size() { return vec2i(width, height); }
}

struct ScrollEvent
{
    double x;
    double y;

    @property vec2 delta() { return vec2(x,y); }
}

struct FocusEvent
{
    bool focused;
}


private extern(C) void glfwCallback(EventType)(GLFWwindow *window, FieldTypeTuple!EventType eventData)
{
    Shell shell = ShellWindow(window).userPointer;
    EventType event = EventType(eventData);
 
    alias ref CallbackQueue!(EventType) QueueType;
    enum eventTypeLowerCase = toLower(typeof(event).stringof[0..1]) ~ typeof(event).stringof[1..$];
    
    QueueType queue = mixin("shell." ~ eventTypeLowerCase ~ "Queue");
    queue(event);
}
