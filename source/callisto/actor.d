/++
 + Provides functionality for entities that have a model transformation.
 + Model transformation may include any of the following:
 + 
 + - Position
 + - Scale
 + - Orientation
 + - Additional transformation matrix
 +
 + This functionality is provided via a mixin template called Transform
 + and can be included in any class or struct.
 +
 + The module also exposes a struct Actor that contains transformation
 + as well as a mesh and can be passed to the render method of a renderer.
 +/
module callisto.actor;

public
{
    import std.typecons : Flag, Yes, No;
    import gl3n.linalg;
    import callisto.mesh;
}

/++
 + An object in space with a mesh and a transform.
 +
 + Examples:
 + -----------------------------
 + import callisto.actor, callisto.primitives;
 + Actor quad = Actor(quad());
 + shell.renderQueue ~= (lag) { renderer.render(quad); };
 + -----------------------------
 +/
struct Actor
{
    mixin Transform;
    Mesh mesh;

    this(Mesh mesh)
    {
        this.mesh = mesh;
    }
}

class SceneNode
{
    mixin Transform!(Yes.hasPosition,
                     Yes.hasScale,
                     Yes.hasOrientation,
                     Yes.hasParent,
                     No.hasPostTransform,
                     updateTransform);
    Mesh mesh;
    SceneNode[] children;

    this()
    {
        updateTransform();
    }

    void addChild(SceneNode child)
    {
        children ~= child;
        child.parentTransform = model;
        child.updateTransform();
    }

    void updateTransform()
    {
        foreach(child; children)
        {
            child.parentTransform = model;
            child.updateTransform();
        }
    }
}

/++
 + Provides transform functionality with a position, scale and orientation.
 + If enabled, a custom additional parent transformation can be enabled with
 + a flag on instantiation.
 +
 + A callback can be specified to react to changes to the transformation.
 +
 + Be aware that properties are generated for transformation fields.
 + This makes the whenChanged callback possible but also means that
 + fields are copied when get can not be edited in place.
 +
 + Examples:
 + -----------------------------
 + struct Guy
 + {
 +     // Enable all transform features
 +     mixin Transform!(Yes.hasPosition,
 +                      Yes.hasScale,
 +                      Yes.hasOrientation,
 +                      No.hasParent,
 +                      No.hasPostTransform
 +                      update);
 +
 +     void update() { /* Perform tasks when transform changed */ }
 + }
 +
 + void main()
 + {
 +     Guy guy;
 +     guy.position = vec3(100, 2, 0);
 +     guy.scale = vec3(100,100,100);
 +
 +     //   guy.position.x += 100; // WRONG, you are editing a copy
 +     guy.position = guy.position + vec3(100,0,0); // works
 + }
 + -----------------------------
 +/
mixin template Transform(Flag!"hasPosition" hasPosition=Yes.hasPosition,
                         Flag!"hasScale" hasScale=Yes.hasScale,
                         Flag!"hasOrientation" hasOrientation=Yes.hasOrientation,
                         Flag!"hasParent" hasParent=No.hasParent,
                         Flag!"hasPostTransform" hasPostTransform=No.hasPostTransform,
                         alias whenChanged={})
{
    static if(hasPosition)
    {
        protected vec3 position_ = vec3(0,0,0);
        @property vec3 position() { return position_; }
        @property void position(vec3 position)
        {
            position_ = position;
            whenChanged();
        }
    }
    static if(hasScale)
    {
        protected vec3 scale_ = vec3(1,1,1);
        @property vec3 scale() { return scale_; }
        @property void scale(vec3 scale)
        {
            scale_ = scale;
            whenChanged();
        }
    }
    static if(hasOrientation)
    {
        protected quat orientation_ = quat(0, vec3(0,0,1));
        @property quat orientation() { return orientation_; }
        @property void orientation(quat orientation)
        {
            orientation_ = orientation;
            whenChanged();
        }
    }
    static if(hasParent)
    {
        protected mat4 parentTransform_ = mat4.identity;
        @property mat4 parentTransform() { return parentTransform_; }
        @property void parentTransform(mat4 parentTransform)
        {
            parentTransform_ = parentTransform;
            whenChanged();
        }
    }
    static if(hasPostTransform)
    {
        protected mat4 postTransform_ = mat4.identity;
        @property mat4 postTransform() { return postTransform_; }
        @property void postTransform(mat4 postTransform)
        {
            postTransform_ = parentTransform;
            whenChanged();
        }
    }

    /++
     + Returns a model transformation matrix from the transform features
     + of this object, ready to set as the model matrix in a shader.
     +/
    @property mat4 model()
    {
        mat4 model = mat4.identity;

        static if(hasParent)
            model = parentTransform_;
        static if(hasOrientation)
            model = orientation_.to_matrix!(4, 4)() * model;
        static if(hasPosition)
            model = mat4.translation(position_.x, position_.y, position_.z) * model;
        static if(hasScale)
            model = mat4.scaling(scale_.x, scale_.y, scale_.z) * model;
        static if(hasPostTransform)
            model = postTransform_ * model;

        return model;
    }

    static if(hasOrientation)
    {
        @property vec3 worldPosition()
        {
            return vec3(model * vec4(position, 1));
        }

        /++ Calculates a normalized forward vector from the orientation +/
        @property vec3 forward()
        {
            return orientation_ * vec3(0,0,-1);
        }

        /++ Calculates a normalized up vector from the orientation +/
        @property vec3 up()
        {
            return orientation_ * vec3(0,1,0);
        }

        /++
         + Sets the orientation with two orthogonal vectors. Orthogonality is not
         + enforced, be sure to pass correct vectors.
         +
         + Params:
         +       forward = the direction that the z-facing side should point to
         +       up = the direction that the top of the object should point to
         +/
        void setOrientation(vec3 forward, vec3 up)
        {
            /// See: http://answers.unity3d.com/questions/467614/what-is-the-source-code-of-quaternionlookrotation.html
            /// Creates a rotation matrix and constructs a quaternion from that

            forward.normalize();
 
            vec3 vector = forward;
            vec3 vector2 = cross(up, vector).normalized;
            vec3 vector3 = cross(vector, vector2);
            auto m00 = vector2.x;
            auto m01 = vector2.y;
            auto m02 = vector2.z;
            auto m10 = vector3.x;
            auto m11 = vector3.y;
            auto m12 = vector3.z;
            auto m20 = vector.x;
            auto m21 = vector.y;
            auto m22 = vector.z;

            float num8 = (m00 + m11) + m22;
            orientation_ = quat();
            if (num8 > 0f)
            {
                auto num = sqrt(num8 + 1f);
                orientation_.w = num * 0.5f;
                num = 0.5f / num;
                orientation_.x = (m12 - m21) * num;
                orientation_.y = (m20 - m02) * num;
                orientation_.z = (m01 - m10) * num;
            }
            else if ((m00 >= m11) && (m00 >= m22))
            {
                auto num7 = sqrt(((1f + m00) - m11) - m22);
                auto num4 = 0.5f / num7;
                orientation_.x = 0.5f * num7;
                orientation_.y = (m01 + m10) * num4;
                orientation_.z = (m02 + m20) * num4;
                orientation_.w = (m12 - m21) * num4;
            }
            else if (m11 > m22)
            {
                auto num6 = sqrt(((1f + m11) - m00) - m22);
                auto num3 = 0.5f / num6;
                orientation_.x = (m10+ m01) * num3;
                orientation_.y = 0.5f * num6;
                orientation_.z = (m21 + m12) * num3;
                orientation_.w = (m20 - m02) * num3;
            }
            else
            {
                auto num5 = sqrt(((1f + m22) - m00) - m11);
                auto num2 = 0.5f / num5;
                orientation_.x = (m20 + m02) * num2;
                orientation_.y = (m21 + m12) * num2;
                orientation_.z = 0.5f * num5;
                orientation_.w = (m01 - m10) * num2;
            }

            whenChanged();
        }
    }
}
