module  callisto.error;

public
{
    import derelict.opengl3.gl3;
}

private
{
    import std.conv;
    import std.string : format;
}

/**
 * Checks for an error and terminates application if an error is set.
 * Error codes mapped to more specific error messages can be passed as a
 * parameter
 */
void checkErrorsOpenGL(string errorInMsg="OpenGL error in", string[GLenum] errorMessages=null, string file=__FILE__, int line=__LINE__) {
    debug {
        GLenum err = glGetError();
        string msg = format("%s %s(%s)\n%s",
                            errorInMsg,
                            file,
                            line,
                            (err in errorMessages)
                               ? errorMessages[err]
                               : err.errorString);

        assert(err == GL_NO_ERROR, msg);
    }
}

@property string errorString(GLenum error) {
    switch(error) {
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument. " ~
               "The offending command is ignored and has no other side effect than to set the error flag.";
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE: A numeric argument is out of range. The offending command is ignored and has no other " ~
               "side effect than to set the error flag.";
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION: The specified operation is not allowed in the current state. The offending command is " ~
               "ignored and has no other side effect than to set the error flag.";
    /*case GL_STACK_OVERFLOW:
        return "GL_STACK_OVERFLOW: This command would cause a stack overflow. The offending command is ignored and has no " ~
               "other side effect than to set the error flag.";
    case GL_STACK_UNDERFLOW:
        return "GL_STACK_UNDERFLOW: This command would cause a stack underflow. The offending command is ignored and has no other side effect " ~
               "than to set the error flag.";
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command. The state of the GL is undefined, except for the " ~
               "state of the error flags, after this error is recorded.";
    case GL_TABLE_TOO_LARGE:
        return "GL_TABLE_TOO_LARGE: The specified table exceeds the implementation's maximum supported table size. The offending command is " ~
               "ignored and has no other side effect than to set the error flag.";*/
    default:
        return null;
    }
}
