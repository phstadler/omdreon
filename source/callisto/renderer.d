module callisto.renderer;

public
{
    import callisto.source;
    import callisto.mesh;
    import callisto.camera;
}

private
{
    import std.stdio;
    import std.range : zip;
    import callisto.shell;
    import callisto.glfw;
    import callisto.shader;
    import callisto.img;
    import callisto.error;
}

enum BlendMode
{
    none,
    additive,
    multiplicative
}

class RenderTarget
{
    this(vec2i size)
    {
        size_ = size;
        generateFrameBufferObject();
        checkErrorsOpenGL();
    }

    void generateFrameBufferObject()
    {
        glGenFramebuffers(1, &fboName);
        glBindFramebuffer(GL_FRAMEBUFFER, fboName);

        colorTex_ = Texture.blank(
            size_,
            GL_TEXTURE_2D,
            GL_RGBA,
            GL_RGBA,
            [
                GL_TEXTURE_MIN_FILTER: GL_LINEAR,
                GL_TEXTURE_MAG_FILTER: GL_LINEAR,
            ]
        );
        glFramebufferTexture2D(
            GL_FRAMEBUFFER,
            GL_COLOR_ATTACHMENT0,
            GL_TEXTURE_2D,
            colorTex_.name,
            0
        );

        depthTex_ = Texture.blank(
            size_,
            GL_TEXTURE_2D,
            GL_DEPTH_COMPONENT24,
            GL_DEPTH_COMPONENT,
            [
                GL_TEXTURE_MIN_FILTER: GL_LINEAR,
                GL_TEXTURE_MAG_FILTER: GL_LINEAR,
            ]
        );
        glFramebufferTexture2D(
            GL_FRAMEBUFFER,
            GL_DEPTH_ATTACHMENT,
            GL_TEXTURE_2D,
            depthTex_.name,
            0
        );

        glDrawBuffers(1, [ GL_COLOR_ATTACHMENT0 ].ptr);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            throw new Exception("Error creating framebuffer");
        }

        currentTarget = this;
    }

    ~this()
    {
        glDeleteFramebuffers(1, &fboName);
        glDeleteRenderbuffers(1, &depthRenderBufferName);
        colorTex_.dispose();
    }

    void bind()
    {
        if(currentTarget !is this)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, fboName);
            glViewport(0, 0, size_.x, size_.y);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            currentTarget = this;
        }
    }

    @property Texture colorTex() { return colorTex_; }
    @property Texture depthTex() { return depthTex_; }

private:
    static RenderTarget currentTarget;
    vec2i size_;
    GLuint fboName;
    GLuint depthRenderBufferName;
    Texture colorTex_;
    Texture depthTex_;
}

/++
 + Everything that is either itself a mesh or exposes a mesh through a .mesh
 + property, is deemed renderable.
 +/
template isRenderable(Type)
{
    enum isRenderable = is(Type == Mesh) || is(typeof(Type.mesh) == Mesh);
}

/++
 + Holds an OpenGL render state with a shader, textures and a camera.
 + Sets transformation matrices for the shader and reloads shaders if
 + they are changed on disk.
 +
 + Can be used to render meshes.
 +/
class Renderer
{
    this(Shell shell,
         TextSource vertexShaderSource,
         TextSource fragmentShaderSource,
         TextSource geometryShaderSource=null,
         TextSource[string] shaderIncludes=null,
         Camera camera=new PerspectiveCam(ShellWindow.defaultSize))
    in
    {
        import std.algorithm : startsWith;
        foreach(path, source; shaderIncludes)
        {
            assert(startsWith(path, "/"), "OpenGL Spec requires filenames to be prefixed with /");
        }
    }
    body
    {
        shell_ = shell;
        shaderIncludes_ = shaderIncludes;

        if(shaderIncludes_.length > 0)
        {
            loadShaderIncludes();
            checkErrorsOpenGL();
        }

        setShader(vertexShaderSource, fragmentShaderSource, geometryShaderSource);

        camera_ = camera;
        camera_.resize(shell.window.framebufferSize);
        shell.resizeEventQueue ~= (ResizeEvent e) {
            camera_.resize(e.size);
        };
        shell.updateQueue ~= &update;
    }

    private void loadShaderIncludes()
    {
        loadArbShadingLanguageInclude();
        foreach(path, source; shaderIncludes_)
        {
            string glslCode = source.read();

            const(char)* nameChars = path.ptr;
            GLsizei nameLen = cast(GLsizei) path.length;

            const(char)* glslCodeChars = glslCode.ptr;
            GLsizei glslCodeLen = cast(GLsizei) glslCode.length;

            glNamedStringARB(GL_SHADER_INCLUDE_ARB, nameLen, nameChars, glslCodeLen, glslCodeChars);
        }
    }

    private void loadArbShadingLanguageInclude()
    {
        assert(glfwExtensionSupported("GL_ARB_shading_language_include"));
        glNamedStringARB = cast(da_glNamedStringARB) glfwGetProcAddress("glNamedStringARB");
        glGetNamedStringARB = cast(da_glGetNamedStringARB) glfwGetProcAddress("glGetNamedStringARB");
        glCompileShaderIncludeARB = cast(da_glCompileShaderIncludeARB) glfwGetProcAddress("glCompileShaderIncludeARB");
    }

    void enableBlendMode()
    {
        final switch(blendMode)
        {
            case BlendMode.none:
                glDisable(GL_BLEND);
                break;

            case BlendMode.additive:
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glBlendEquation(GL_FUNC_ADD);
                break;

            case BlendMode.multiplicative:
                glEnable(GL_BLEND);
                glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
                glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);
                break;
        }

    }

    /++ Manages recompiling of changed shaders, should be called each frame +/
    void update()
    {
        bool shouldRecompile = vertexShaderSource_.didChange ||
                               fragmentShaderSource_.didChange ||
                               geometryShaderSource_ !is null && geometryShaderSource_.didChange;

        if(shouldRecompile)
        {
            compileShader();
        }
    }

    void compileShader()
    {
        try
        {
            if(shader_.valid)
            {
                shader_.dispose();
            }
            shader_ = Shader.create(
                vertexShaderSource_.read(),
                fragmentShaderSource_.read(),
                (geometryShaderSource_ !is null)
                    ? geometryShaderSource_.read()
                    : null
            );
        }
        catch(ShaderException e)
        {

            writeln(e.msg);
            shader_ = Shader.create(errorShaderVertex, errorShaderFragment);
        }

        uniforms_ = shader_.uniforms;
    }

    void setShader(TextSource vertexShaderSource, TextSource fragmentShaderSource, TextSource geometryShaderSource=null)
    {
        vertexShaderSource_   = vertexShaderSource;
        fragmentShaderSource_ = fragmentShaderSource;
        geometryShaderSource_ = geometryShaderSource;
        compileShader();
    }

    /++
     + Assigns a datasource containing a FreeImage compatible image to a sampler uniform.
     +
     + Examples:
     + ---------------------
     + renderer.setTexture("myMainTextureSampler", dataResource("textures/1.png"));
     + ---------------------
     +/
    void setTexture(string samplerUniformName, DataSource source)
    {
        setTexture(samplerUniformName, loadTexture(source));
    }

    void setTexture(string samplerUniformName, Texture tex)
    {
        samplerUniformNames ~= samplerUniformName;
        textures ~= tex;
    }

    /++
     + Renders the given renderable objects.
     +
     + A renderable object must either be a mesh or have a property
     + named mesh that returns the mesh to render or be a Mesh itself.
     +
     + If the renderable object has a property named model of type mat4,
     + it is used as the model transformation matrix. Otherwise, the
     + identity matrix is used as the model matrix.
     +
     + Examples:
     + ---------------------
     + import callisto.primitives;
     + renderer.render(quad());
     + ---------------------
     +/
    void render(T)(T[] actors...) if (isRenderable!T)
    out
    {
        checkErrorsOpenGL();
    }
    body
    {
        if(renderTarget !is null)
        {
            renderTarget.bind();
        }
        else if(renderTarget is null && RenderTarget.currentTarget !is null)
        {
            vec2i screenSize = shell_.window.framebufferSize;
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glViewport(0,0,screenSize.x,screenSize.y);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            RenderTarget.currentTarget = null;
        }

        enableBlendMode();

        shader_.use();

        int textureIdx = 0;
        foreach(samplerUniformName, texture; zip(samplerUniformNames, textures))
        {
            texture.use(textureIdx);
            uniforms_[samplerUniformName] = textureIdx;
            ++textureIdx;
        }
        foreach(actor; actors)
        {
            mat4 model;

            static if(is(typeof(actors[0].model) == Matrix!(float, 4, 4)))
                model = actor.model;
            else
                model = mat4.identity;

            mat4 modelView = camera.view * model;
            mat4 modelViewProjection = camera.projection * modelView;
            mat3 normalMatrix = mat3(modelView.inverse.transposed);

            uniforms_["model"] = model;
            uniforms_["view"] = camera.view;
            uniforms_["projection"] = camera.projection;
            uniforms_["modelViewProjection"] = modelViewProjection;
            uniforms_["modelView"] = modelView;
            uniforms_["normalMatrix"] = normalMatrix;

            assignUniforms(intUniforms);
            assignUniforms(floatUniforms);
            assignUniforms(vec2Uniforms);
            assignUniforms(vec3Uniforms);
            assignUniforms(vec4Uniforms);
            assignUniforms(mat3Uniforms);
            assignUniforms(mat4Uniforms);

            Mesh mesh;
            static if(is(T == Mesh))
            {
                mesh = actor;
            }
            else
            {
                mesh = actor.mesh;
            }

            mesh.use(shader_);
            mesh.render();
        }
    }

    @property Camera camera() { return camera_; }

    /++
     + Sets the uniform in the shader if it has a uniform with that name.
     + The uniform will be cached until a new one is set.
     +/
    void setUniform(T)(string name, T value)
    if(is(T == GLint) ||
       is(T == float) ||
       is(T == vec2) ||
       is(T == vec3) ||
       is(T == vec4) ||
       is(T == mat3) ||
       is(T == mat4))
    {
        // use short name mat4 not Matrix(float, 4, 4)
        static if(is(T==mat4)) enum prefix = "mat4";
        else static if(is(T==mat3)) enum prefix = "mat3";
        else static if(is(T==vec4)) enum prefix = "vec4";
        else static if(is(T==vec3)) enum prefix = "vec3";
        else static if(is(T==vec2)) enum prefix = "vec2";
        else static if(is(T==GLint)) enum prefix = "int";
        else enum prefix = T.stringof;
        //static if(is(T==vec2)) enum prefix = "vec2";


        // e.g. renderer.setUniform("UV", vec2(0.5, 0.5)) => vec2Uniforms[name] = value;
        mixin(prefix ~ "Uniforms[name] = value;");
    }

    BlendMode blendMode = BlendMode.none;
    RenderTarget renderTarget;

private:
    void assignUniforms(T)(T[string] uniformValues)
    {
        foreach(name, value; uniformValues)
        {
            uniforms_[name] = value;
        }
    }

    Shell shell_;
    TextSource vertexShaderSource_;
    TextSource fragmentShaderSource_;
    TextSource geometryShaderSource_;
    TextSource[string] shaderIncludes_;
    Shader shader_;
    Uniforms uniforms_;
    Camera camera_;

    string[] samplerUniformNames;
    Texture[] textures;

    GLint[string] intUniforms;
    float[string] floatUniforms;
    vec2[string] vec2Uniforms;
    vec3[string] vec3Uniforms;
    vec4[string] vec4Uniforms;
    mat3[string] mat3Uniforms;
    mat4[string] mat4Uniforms;

    static
    {
        immutable errorShaderVertex = "
            #version 330 core

            uniform mat4 modelViewProjection;

            in vec3 position;
            in vec3 normal;
            in vec2 texCoords;

            void main()
            {
                gl_Position = modelViewProjection * vec4(position, 1);
            }
        ";

        immutable errorShaderFragment = "
            #version 330 core

            out vec4 color;

            void main()
            {
                color = vec4(1, 0, 0, 1);
            }
        ";
    }
}
