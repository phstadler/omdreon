module callisto.primitives;

public
{
    import callisto.mesh;
}

private
{
    import gl3n.linalg;
    import std.math;
    import std.traits;
    import std.algorithm;
    import std.range;
    import std.typecons : Nullable;
}

enum nullAttr = MeshAttr(); // default initialized => valid yields false
enum positionDefault = MeshAttr("position", 3);
enum normalDefault   = MeshAttr("normal", 3);
enum texCoordDefault = MeshAttr("texCoords", 2);

class MeshCreator(Attrs...) if(isTypeTuple!(Attrs))
{
    GLushort putVertex(Attrs vertexData)
    {
        foreach(vertexDataElement; vertexData)
        {
            vertices_ ~= vertexDataElement.vector;
        }
        return nextIdx++;
    }
    unittest
    {
        auto creator = new MeshCreator!(vec3,vec2)();
        auto idx1 = creater.putVertex(vec3(), vec2());
        auto idx2 = creater.putVertex(vec3(), vec2());
        assert(idx1 == 0 && idx2 == 1);
    }

    void putTriangle(vec3i indexes)
    {
        indexes_ ~= cast(GLushort[]) indexes.vector;
    }

    void putIndex(GLushort index)
    {
        indexes_ ~= index;
    }

    void putIndexes(GLushort[] indexes...)
    {
        foreach(idx; indexes) indexes_ ~= idx;
    }

    void putTriangle(GLushort vertIdx1, GLushort vertIdx2, GLushort vertIdx3)
    {
        putIndexes(vertIdx1, vertIdx2, vertIdx3);
    }

    GLushort[3] putTriangle(Attrs vert1, Attrs vert2, Attrs vert3)
    {
        return [
            putVertex(vert1),
            putVertex(vert2),
            putVertex(vert3)
        ];
    }

    @property static GLint stride()
    {
        GLint stride;
        foreach(Attr; Attrs)
        {
            stride += Attr.dimension;
        }
        return stride;
    }
    unittest
    {
        assert(new MeshCreator!(vec3,vec4,vec2)().stride == 3+4+2);
    }

    auto eachVertex()
    {
        return map!(index => vertices_[index*stride..(index+1)*stride])(indexes_);
    }

    @property GLfloat[] vertices() { return vertices_; }
    @property GLushort[] indexes() { return indexes_; }

private:
    GLfloat[] vertices_;
    GLushort[] indexes_;
    GLushort nextIdx;
}

Mesh icosahedron(float size=1, MeshAttr positionAttr=positionDefault, MeshAttr normalAttr=normalDefault)
{
    GLfloat[] vertices;
    GLushort[] indexes;

    icosahedronGeometry(vertices, indexes);
    int stride = positionAttr.size + normalAttr.size;
    scaleVertices(vertices, stride, size);

    return new Mesh(vertices, indexes, GL_TRIANGLES, positionAttr, normalAttr);
}

/++
 + Creates a sphere by subdividing the faces of a icosahedron.
 + The divisions parameter will determine how smooth the sphere should be,
 + more divisions generate more geometry but a smoother sphere.
 +
 + Examples:
 + ----------------------------------------------
 + Mesh sphere = icosphere(10); // Create a sphere with radius 10
 + ----------------------------------------------
 +/
Mesh icosphere(float size=1, int divisions=10, MeshAttr positionAttr=positionDefault, MeshAttr normalAttr=normalDefault, MeshAttr texCoordAttr=texCoordDefault)
{
    auto positionMesh = new MeshCreator!vec3();
    int icosahedronStride = positionAttr.size + normalAttr.size;

    // icosahedron verts and indexes
    GLfloat[] baseVertices;
    GLushort[] baseIndexes;
    icosahedronGeometry(baseVertices, baseIndexes);

    for(int i = 0; i < baseIndexes.length; i += 3) {
        size_t pointIdx;

        pointIdx = baseIndexes[i]*icosahedronStride;
        vec3 p1 = baseVertices[pointIdx..pointIdx+3];
        pointIdx = baseIndexes[i+1]*icosahedronStride;
        vec3 p2 = baseVertices[pointIdx..pointIdx+3];
        pointIdx = baseIndexes[i+2]*icosahedronStride;
        vec3 p3 = baseVertices[pointIdx..pointIdx+3];

        subdivideTriangle(p1, p2, p3, divisions, positionMesh);
    }

    GLfloat[] icosahedronPositions = positionMesh.vertices;
    GLushort[] icosahedronIndices;

    //size_t vertexCount = icosahedronPositions.length / 3;
    //int sphereStride = positionAttr.size + normalAttr.size + texCoordAttr.size;
    // 3 position coordinates, 3 normal coordinates, 2 UV coordinates
    //GLfloat[] icosahedronVertices = new GLfloat[vertexCount*sphereStride];
    auto icosphereCreator = new MeshCreator!(vec3,vec3,vec2)();
    Nullable!vec2 lastUV;

    foreach(vec3 position; positionMesh.eachVertex)
    {
        vec2 uv = unitSphereUV(position); // position is normalized

        if(!lastUV.isNull)
        {
            vec2 uvDiff = uv - lastUV;
            if(abs(uvDiff.x) > 0.8)
            {
                uv.x += -sgn(uvDiff.x);
            }

            if(abs(uvDiff.y) > 0.8)
            {
                uv.y += -sgn(uvDiff.y);
            }
        }

        GLushort idx = icosphereCreator.putVertex(position*size,
                                                  position,
                                                  uv);
        icosphereCreator.putIndex(idx);
        lastUV = uv;
    }

    return new Mesh(icosphereCreator.vertices, icosphereCreator.indexes, GL_TRIANGLES, positionAttr, normalAttr, texCoordAttr);
}

private vec2 unitSphereUV(vec3 position)
{
    vec3 toCenter = -position;
    return vec2(
        0.5 + atan2(toCenter.z, toCenter.x) / (2*PI),
        0.5 - asin(toCenter.y) / PI
    );
}

/++
 + Subdivides a triangle with an arbitrary number of divisions
 +
 + See:
 +        http://donhavey.com/blog/tutorials/tutorial-3-the-icosahedron-sphere/
 +/
private void subdivideTriangle(vec3 p1, vec3 p2, vec3 p3, uint divisions, MeshCreator!(vec3) mesh)
in
{
    assert(divisions >= 2, "Dividing a triangle needs at least two divisions");
}
body
{
    vec3[] side12Positions = new vec3[divisions+1];
    GLushort[] side12Indexes = new GLushort[divisions+1];
    side12Positions[0] = p1.normalized;
    side12Indexes[0] = mesh.putVertex(side12Positions[0]);
    side12Positions[divisions] = p2.normalized;
    side12Indexes[divisions] = mesh.putVertex(side12Positions[divisions]);

    vec3[] side13Positions = new vec3[divisions+1];
    GLushort[] side13Indexes = new GLushort[divisions+1];
    side13Positions[0] = p1.normalized;
    side13Indexes[0] = mesh.putVertex(side13Positions[0]);
    side13Positions[divisions] = p3.normalized;
    side13Indexes[divisions] = mesh.putVertex(side13Positions[divisions]);

    vec3[][] spanPositions = new vec3[][divisions];
    GLushort[][] spanIndexes = new GLushort[][divisions];
    foreach(ref vec3[] spanRow; spanPositions) { spanRow = new vec3[divisions+1]; }
    foreach(ref GLushort[] spanRow; spanIndexes) { spanRow = new GLushort[divisions+1]; }

    // calculate side vertices
    for(int i=1;i<divisions;i++)
    {
        side12Positions[i] = (p1 + i/float(divisions) * (p2-p1)).normalized;
        side12Indexes[i] = mesh.putVertex(side12Positions[i]);
        side13Positions[i] = (p1 + i/float(divisions) * (p3-p1)).normalized;
        side13Indexes[i] = mesh.putVertex(side13Positions[i]);
    }

    // calculate middle vertices
    for(int i=0;i<divisions+1;i++)
    {
        for(int j=1;j<divisions-i;j++)
        {
            spanPositions[i][j] = (side12Positions[divisions-i] + (j/float(divisions-i)) * (side13Positions[divisions-i] - side12Positions[divisions-i])).normalized;
            spanIndexes[i][j] = mesh.putVertex(spanPositions[i][j]);
        }
    }

    //top four subfaces
    mesh.putTriangle(side12Indexes[0], side12Indexes[1], side13Indexes[1]);
    mesh.putTriangle(side12Indexes[1], side12Indexes[2], spanIndexes[divisions-2][1]);
    mesh.putTriangle(side12Indexes[1], spanIndexes[divisions-2][1], side13Indexes[1]);
    mesh.putTriangle(side13Indexes[1], spanIndexes[divisions-2][1], side13Indexes[2]);

    //the rest of the subfaces
    for(int i=2;i<divisions;i++)
    {
        //side 2
        mesh.putTriangle(side12Indexes[i], side12Indexes[i+1], spanIndexes[divisions-i-1][1]);
        mesh.putTriangle(side12Indexes[i], spanIndexes[divisions-i-1][1], spanIndexes[divisions-i][1]);
        //center
        for(int j=1;j<divisions-i+1;j++)
        {
            mesh.putTriangle(spanIndexes[i-2][j+1], spanIndexes[i-1][j], spanIndexes[i-2][j]);
            if(i>2)
            {
                mesh.putTriangle(spanIndexes[i-2][j+1], spanIndexes[i-2][j], spanIndexes[i-3][j+1]);
            }
        }
        //side 2
        mesh.putTriangle(side13Indexes[i], spanIndexes[divisions-i-1][i], side13Indexes[i+1]);
        mesh.putTriangle(side13Indexes[i], spanIndexes[divisions-i][i-1], spanIndexes[divisions-i-1][i]);
    }
}

/++
 + Creates a triangle mesh with the given width and height.
 + Examples:
 + ----------------------------------------------
 + Mesh tri = triangle(0.1, 0.3);
 + ----------------------------------------------
 +/
Mesh triangle(float width=1,
              float height=1,
              MeshAttr positionAttr=positionDefault,
              MeshAttr texCoordAttr=texCoordDefault)
in
{
    assert(positionAttr.valid, "Position attribute cannot be omitted and must be valid");
    assert(positionAttr.size == 3, "Position attribute supports only vec3 at the moment, sorry");
    assert(!texCoordAttr.valid || (texCoordAttr.size == 2),
           "Texture coordinate attribute supports only vec2 at the moment, sorry");
}
body
{
    MeshAttr[] attributes;
    GLfloat[] vertices;

    width /= 2;
    height /= 2;
    
    if(texCoordAttr.valid)
    {
        attributes = [ positionAttr, texCoordAttr ];
        vertices = [
            0,  height,  0,
            0.5, 1,
            -width, -height, 0,
            0, 0,
            width,  -height, 0,
            1, 0
        ];
    } else {
        attributes = [ positionAttr ];
        vertices = [
            0,  height,  0,
            -width, -height, 0,
            width,  -height, 0
        ];
    }

    return new Mesh(
        vertices,
        null,
        GL_TRIANGLES,
        attributes
    );
}

/++
 + Creates a quad mesh with the given width and height.
 + Examples:
 + ----------------------------------------------
 + Mesh quad = quad(0.4, 0.4);
 + ----------------------------------------------
 +/
Mesh quad(float width=1,
          float height=1,
          MeshAttr positionAttr=positionDefault,
          MeshAttr normalAttr=normalDefault,
          MeshAttr texCoordAttr=texCoordDefault)
in
{
    assert(positionAttr.valid, "Position attribute cannot be omited and must be valid");
    assert(positionAttr.size == 3, "Position attribute supports only vec3 at the moment, sorry");
    assert(!texCoordAttr.valid || (texCoordAttr.size == 2),
           "Texture coordinate attribute supports only vec2 at the moment, sorry");
}
body
{
    MeshAttr[] attributes;
    GLfloat[] vertices;
    GLushort[] indices;

    width /= 2;
    height /= 2;

    vertices = [
        -width, -height, 0,
        0,0,1,
        0,0,
        width, -height, 0,
        0,0,1,
        1,0,
        width, height, 0,
        0,0,1,
        1,1,
        -width, height, 0,
        0,0,1,
        0,1
    ];

    indices = [
        0, 2, 3,
        0, 1, 2
    ];

    return new Mesh(
        vertices,
        indices,
        GL_TRIANGLES,
        positionAttr, normalAttr, texCoordAttr
    );  
}

/++
 + Creates a circle mesh with the given radius.
 + The circle consists only of the outline, not its interior.
 +
 + Examples:
 + ----------------------------------------------
 + Mesh circle = circle(0.4);
 + ----------------------------------------------
 +/
Mesh circle(float radius=0.5, int divisions=24)
{
    immutable(float)[] verts;
    quat rotation = quat.zrotation((2*PI) / divisions);
    vec3 vert = vec3(0,radius,0);

    foreach(i; 0..divisions)
    {
        verts ~= vert.x;
        verts ~= vert.y;
        verts ~= 0;

        vert = rotation * vert;
    }

    return new Mesh(verts, null, GL_LINE_LOOP, [positionDefault]);
}

private void scaleVertices(ref GLfloat[] vertices, int stride, float scale)
{
    for(int i = 0; i < vertices.length; i += stride)
    {
        vertices[i..i+3] *= scale; // scale positions by size
    }
}

/++
 + Creates an indexed icosahedron mesh with normals and size 1
 +/
private void icosahedronGeometry(out GLfloat[] vertices, out GLushort[] indices)
{
    real t = (1.0 + sqrt(5.0)) / 2.0;

    vertices = [
        -1,  t,  0,
        -1,  t,  0, // normals are same as positions
        1,  t,  0,
        1,  t,  0,
        -1, -t,  0,
        -1, -t,  0,
        1, -t,  0,
        1, -t,  0,
        0, -1,  t,
        0, -1,  t,
        0,  1,  t,
        0,  1,  t,
        0, -1, -t,
        0, -1, -t,
        0,  1, -t,
        0,  1, -t,
        t,  0, -1,
        t,  0, -1,
        t,  0,  1,
        t,  0,  1,
        -t,  0, -1,
        -t,  0, -1,
        -t,  0,  1,
        -t,  0,  1
    ];

    indices = [
        0, 11, 5,
        0, 5, 1,
        0, 1, 7,
        0, 7, 10,
        0, 10, 11,

        1, 5, 9,
        5, 11, 4,
        11, 10, 2,
        10, 7, 6,
        7, 1, 8,

        3, 9, 4,
        3, 4, 2,
        3, 2, 6,
        3, 6, 8,
        3, 8, 9,

        4, 9, 5,
        2, 4, 11,
        6, 2, 10,
        8, 6, 7,
        9, 8, 1,
    ];
}
