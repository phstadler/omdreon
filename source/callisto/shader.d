module callisto.shader;

public {
    import derelict.opengl3.gl3;
    import gl3n.linalg;
}

private {
    import std.string : toStringz;
    import std.array : join;
    import std.stdio;
    import std.algorithm : sort;
    import std.typecons : Nullable;
    import callisto.error;
}

/++
 + Encapsulates a shader program
 +/
struct Shader
{
    @property static Shader current()
    {
        GLint name;
        glGetIntegerv(GL_CURRENT_PROGRAM, &name);

        if(name == 0) {
            throw new ShaderException("No shader is currently in use");
        }

        return Shader(name);
    }

    static Shader create(string vertexShaderSource, string fragmentShaderSource, string geometryShaderSource=null)
    out
    {
        assert(glGetError() == GL_NO_ERROR, "OpenGL signalled error");
    }
    body
    {
        Shader shader;
        shader.name = glCreateProgram();

        GLuint vertex   = compileShader(vertexShaderSource,   GL_VERTEX_SHADER);
        GLuint fragment = compileShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
        GLuint geometry;

        glAttachShader(shader.name, vertex);
        glAttachShader(shader.name, fragment);
        if(geometryShaderSource !is null)
        {
            geometry = compileShader(geometryShaderSource, GL_GEOMETRY_SHADER);
            glAttachShader(shader.name, geometry);
        }

        glLinkProgram(shader.name);

        GLint isLinked = 0;
        glGetProgramiv(shader.name, GL_LINK_STATUS, &isLinked);
        if(isLinked == GL_FALSE)
        {
            GLint maxLength = 0;
            glGetProgramiv(shader.name, GL_INFO_LOG_LENGTH, &maxLength);

            char[] log = new char[maxLength];
            glGetProgramInfoLog(shader.name, maxLength, &maxLength, log.ptr);

            //We don't need the shader program anymore.
            glDeleteProgram(shader.name);
            //Don't leak shaders either.
            glDeleteShader(vertex);
            glDeleteShader(fragment);
            if(geometryShaderSource !is null)
            {
                glDeleteShader(geometry);
            }

            throw new ShaderException("Shader program could not be linked:\n" ~ log.idup);
        }

        //Always detach shaders after a successful link.
        glDetachShader(shader.name, vertex);
        glDetachShader(shader.name, fragment);
        if(geometryShaderSource !is null)
        {
            glDetachShader(shader.name, geometry);
        }

        return shader;
    }

    /++
     + Compiles a shader of the given type using the  given source code.
     + If successful, the name (GLuint) of the compiled shader is
     + returned.
     +
     + Params:
     +      type =     Type of the shader as OpenGL enum, e.g. GL_VERTEX_SHADER
     +      filename = path to a file containing the source code of the shader
     +
     + Throws: ShaderException if compilation fails
     +/
    static GLuint compileShader(string source, GLenum type)
    out
    {
        checkErrorsOpenGL("Error compiling shader in");
    }
    body
    {
        const(char)* shaderSource   = toStringz(source);
        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, &shaderSource, null);
        glCompileShader(shader);

        GLint isCompiled = GL_FALSE;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if(isCompiled == GL_FALSE)
        {
            GLint maxLength = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

            char[] log = new char[maxLength];

            glGetShaderInfoLog(shader, maxLength, &maxLength, log.ptr);

            glDeleteShader(shader);

            throw new ShaderException(format("%s could not be compiled:\n%s", shaderTypeName(type), log.idup));
        }

        return shader;
    }

    private static string shaderTypeName(GLenum type)
    {
        string shaderTypeName;
        switch(type)
        {
            case GL_VERTEX_SHADER:
                shaderTypeName = "Vertex Shader";
                break;
            case GL_FRAGMENT_SHADER:
                shaderTypeName = "Fragment Shader";
                break;
            case GL_GEOMETRY_SHADER:
                shaderTypeName = "Geometry Shader";
                break;
            default:
                shaderTypeName = "Shader";
        }
        return shaderTypeName;
    }

    this(GLint initialName)
    {
        name = initialName;
    }

    /++ OpenGL-name of the shader program +/
    Nullable!(GLint, GLint.min) name;

    /++ Return true if the shader has been initialized and is not yet disposed +/
    @property bool valid()
    {
        return !name.isNull;
    }

    @property bool inUse()
    {
        return Shader.current.name == name;
    }

    @property Uniforms uniforms()
    {
        return Uniforms.create(this);
    }

    @property Attributes attributes()
    {
        return Attributes.create(this);
    }

    void dispose()
    in
    {
        assert(!name.isNull, "Trying to dispose an uninitialized shader");
    }
    out
    {
        checkErrorsOpenGL(format("Error disposing shader with name %s in ", name));
    }
    body
    {
        glDeleteShader(name);
        name = name.init;
    }

    Uniform getUniform(string uniformName)
    in
    {
        assert(name != -1, "Trying to get a uniform for an uninitalized shader");
    }
    out
    {
        assert(glGetError() == GL_NO_ERROR, "OpenGL signalled error");
    }
    body
    {
        Uniform u;
        u.location = uniformLocation(uniformName);
        u.name = uniformName;

        GLuint index = uniformIndex(uniformName);

        glGetActiveUniform(name, index, 0, null, &u.size, &u.type, null);

        return u;
    }

    /**
     * Makes the shader active
     */
    void use()
    in
    {
        assert(name != -1, "Trying to use an uninitalized shader");
    }
    out
    {
        assert(glGetError() == GL_NO_ERROR, "OpenGL signalled error");
    }
    body
    {
        glUseProgram(name);
    }

    GLint attributeLocation(string attributeName)
    {
        return glGetAttribLocation(name, toStringz(attributeName));
    }

    GLint uniformLocation(string uniformName)
    {
        return glGetUniformLocation(name, toStringz(uniformName));
    }

    GLuint uniformIndex(string uniformName) {
        return glGetProgramResourceIndex(name, GL_UNIFORM, toStringz(uniformName));
    }

    void uniformMatrix4f(GLint uniformLocation, mat4 matrix)
    {
        // According to OpenGL doc, transpose must be GL_FALSE and will generate an
        // error if set to GL_TRUE, transposing in advance
        glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, matrix.transposed().value_ptr);
    }
}

struct Uniforms {
    public static Uniforms create(Shader shader) {
        Uniforms u;
        enum uniformNameMaxLen = 128;

        int total = -1;
        glGetProgramiv(shader.name, GL_ACTIVE_UNIFORMS, &total);
        for(GLuint i=0; i<total; ++i)  {
            Uniform uniform;

            int nameLen=-1;
            char[uniformNameMaxLen] name;
            glGetActiveUniform(shader.name, i, uniformNameMaxLen, &nameLen, &uniform.size, &uniform.type, name.ptr);
            uniform.name = name[0..nameLen].idup;
            uniform.location = shader.uniformLocation(uniform.name);

            u.uniforms ~= uniform;
        }

        sort(u.uniforms);
        return u;
    }

    Uniform[] uniforms;

    /++
     + Gets the uniform with the given name.
     +
     + If the required parameter is not specified
     + or set to true, trying to get a uniform
     + that is not present in the shader will throw
     + a ShaderException.
     +
     + If the required parameter is set to false
     + and the uniform is not present an invalid
     + uniform is returned.
     + This uniform cannot be assigned to, be sure to
     + check the valid property before assigning.
     +
     + Examples:
     + -------------
     + Uniforms u = shader.uniforms;
     + Uniform tint = u.get("u_optionalTint", false);
     + if(tint.valid)
     + {
     +     tint = vec4(1, 0, 0, 1);
     + }
     +
     + Uniform invalid = u.get("u_invalidUniform"); // throws ShaderException
     + -------------
     +/
    Uniform get(string name, bool required=true)
    {
        foreach(u; uniforms)
        {
            if(u.name == name)
            {
                return u;
            }
        }

        if(required)
        {
            throw new ShaderException("Uniform with name " ~ name ~ " was not found");
        }
        else
        {
            return Uniform(); // invalid uniform
        }
    }

    void set(T)(string name, T value, bool required=true)
    {
        Uniform uniform = get(name, required);
        if(uniform.valid) // if not required and not present
        {
            uniform = value;
        }
    }

    /++
     + Sets the uniform with the given name.
     + If the uniform does not exist in the shader, the call
     + is silently ignored.
     +
     + If this case is an error in your use case,
     + do not use index assignment. Instead, assign using
     + a property
     +/
    T opIndexAssign(T)(T value, string name)
    {
        set(name, value, false);
        return value;
    }

    @property Uniform opDispatch(string uniformName)() const {
        foreach(u; uniforms) {
            if(u.name == uniformName) {
                return u;
            }
        }

        throw new ShaderException("Uniform with name " ~ uniformName ~ " was not found");
    }

     /++
     + Sets the uniform with the given name.
     + If the uniform does not exist in the shader,
     + a ShaderException is thrown
     +/
    @property Uniform opDispatch(string uniformName, T)(T value) {
        set(uniformName, value, true);
    }

    void toString(scope void delegate(const(char)[]) sink) const {
        sink("Uniforms[");
        foreach(i, u; uniforms) {
            u.toString(sink);
            if(i < uniforms.length-1) {
                sink(",");
            }
        }
        sink("]");
    }
}

struct Attributes
{
    static Attributes create(Shader shader) {
        Attributes a;
        enum attributeNameMaxLen = 128;

        int total = -1;
        glGetProgramiv(shader.name, GL_ACTIVE_ATTRIBUTES, &total);
        for(GLuint i=0; i<total; ++i)  {
            Attribute attr;

            int nameLen=-1;
            char[attributeNameMaxLen] name;
            GLenum type;
            GLint size;

            glGetActiveAttrib(shader.name, i, attributeNameMaxLen, &nameLen, &size, &type, name.ptr);
            attr.name = name[0..nameLen].idup;
            attr.location = shader.attributeLocation(attr.name);
            switch(type) {
            case GL_FLOAT_VEC4:
                attr.type = GL_FLOAT;
                attr.size = 4;
                break;
            case GL_FLOAT_VEC3:
                attr.type = GL_FLOAT;
                attr.size = 3;
                break;
            case GL_FLOAT_VEC2:
                attr.type = GL_FLOAT;
                attr.size = 2;
                break;
            case GL_FLOAT_MAT4:
                attr.type = GL_FLOAT;
                attr.size = 4*4;
                break;
            case GL_FLOAT_MAT3:
                attr.type = GL_FLOAT;
                attr.size = 3*3;
                break;
            case GL_FLOAT_MAT2:
                attr.type = GL_FLOAT;
                attr.size = 2*2;
                break;
            case GL_INT_VEC4:
                attr.type = GL_INT;
                attr.size = 4;
                break;
            case GL_INT_VEC3:
                attr.type = GL_INT;
                attr.size = 3;
                break;
            case GL_INT_VEC2:
                attr.type = GL_INT;
                attr.size = 2;
                break;
            case GL_BOOL_VEC4:
                attr.type = GL_BOOL;
                attr.size = 4;
                break;
            case GL_BOOL_VEC3:
                attr.type = GL_BOOL;
                attr.size = 3;
                break;
            case GL_BOOL_VEC2:
                attr.type = GL_BOOL;
                attr.size = 4;
                break;
            default:
                attr.type = type;
                attr.size = 1;
                break;
            }

            a.attributes ~= attr;
        }
        return a;
    }

    Attribute[] attributes;

    Attribute get(string name) {
        foreach(a; attributes) {
            if(a.name == name) {
                return a;
            }
        }
        throw new ShaderException("Attribute with name " ~ name ~ " was not found");
    }

    Attribute[] opSlice() {
        return attributes;
    }

    @property Attribute opDispatch(string attrName)() const {
        foreach(a; attributes) {
            if(a.name == attrName) {
                return a;
            }
        }

        throw new ShaderException("Attribute with name " ~ attrName ~ " was not found");
    }

    void toString(scope void delegate(const(char)[]) sink) const {
        sink("Attributes[");
        foreach(i, a; attributes) {
            a.toString(sink);
            if(i < attributes.length-1) {
                sink(",");
            }
        }
        sink("]");
    }
}

struct Uniform
{
    string name;
    GLint location = -1;
    GLenum type;
    GLint size = -1;

    @property bool valid() { return location != -1; }

    T opAssign(T)(T f) if(is(T == float))
    in
    {
        assert(location != -1, "Tried to assign float to uniform without a valid location");
        assert(type == GL_FLOAT, "Tried to assign float, but uniform type is not GL_FLOAT");
        assert(size == 1, "Tried to assign single float, but size is not 1");
    }
    body
    {
        glUniform1f(location, f);
        return f;
    }

    T opAssign(T)(T i) if(is(T == int))
    in
    {
        assert(location != -1, "Tried to assign int to uniform without a valid location");
        assert(type == GL_INT || type == GL_SAMPLER_2D || type == GL_SAMPLER_3D, "Tried to assign int, but uniform type is not GL_INT or GL_SAMPLER_2D or GL_SAMPLER_3D");
        assert(size == 1, "Tried to assign single int, but size is not 1");
    }
    body
    {
        glUniform1i(location, i);
        return i;
    }

    Vector!(T,L) opAssign(T,int L)(Vector!(T,L) vec) if(is(T == float) && L > 0 && L <= 4)
    in
    {
        assert(location != -1, "Tried to assign float vector to uniform without a valid location");
        assert(type == mixin("GL_FLOAT_VEC" ~ to!string(L)), "Tried to assign float vector, but uniform type is not GL_FLOAT_VEC" ~ L);
        assert(size == 1, "Tried to assign single float vector, but size is not 1");
    }
    body
    {
        mixin("glUniform" ~ to!string(L) ~ "fv(location, 1, vec.value_ptr);");
        return vec;
    }

    Matrix!(T,R,C) opAssign(T,int R,int C)(Matrix!(T,R,C) mat) if(is(T == float) && R == C && R >= 2 && R <= 4)
    in
    {
        assert(location != -1, "Tried to assign a matrix to uniform without a valid location");
        assert(type == mixin("GL_FLOAT_MAT" ~ to!string(R)), "Tried to assign float matrix, but uniform type is not GL_FLOAT_MAT" ~ R);
        assert(size == 1, "Tried to assign single float matrix, but size is not 1");
    }
    body
    {
        mixin("glUniformMatrix" ~ to!string(R) ~ "fv(location, 1, GL_TRUE, mat.value_ptr);");
        return mat;
    }

    int opCmp(ref const Uniform other) const {
        return name < other.name;
    }

    void toString(scope void delegate(const(char)[]) sink) const {
        sink(name);
        sink("[location=");
        sink(to!string(location));
        sink(",type=");
        sink(to!string(type.typeName));
        if(size != 1) {
            sink(",size=");
            sink(to!string(size));
        }
        sink("]");
    }
}

struct Attribute
{
    string name;
    GLint location = -1;
    GLenum type;
    GLint size = -1;

    @property GLsizei stride() const
    {
        return type.stride * size;
    }

    void toString(scope void delegate(const(char)[]) sink) const {
        sink(name);
        sink("[location=");
        sink(to!string(location));
        sink(",type=");
        sink(format("%s", type.typeName));
        sink(",stride=");
        sink(to!string(this.stride));
        if(size != 1) {
            sink(",size=");
            sink(to!string(size));
        }
        sink("]");
    }
}

@property GLsizei stride(Attribute[] attributes)
{
    GLsizei stride;
    foreach(a; attributes)
    {
        stride += a.stride;
    }
    return stride;
}

@property string typeName(GLenum enumeration) {
    switch(enumeration) {
    case GL_FLOAT:
        return "GL_FLOAT";
    case GL_FLOAT_VEC4:
        return "GL_FLOAT_VEC4";
    case GL_FLOAT_VEC3:
        return "GL_FLOAT_VEC3";
    case GL_FLOAT_VEC2:
        return "GL_FLOAT_VEC2";
    case GL_FLOAT_MAT4:
        return "GL_FLOAT_MAT4";
    case GL_FLOAT_MAT3:
        return "GL_FLOAT_MAT3";
    case GL_FLOAT_MAT2:
        return "GL_FLOAT_MAT2";
    case GL_DOUBLE:
        return "GL_DOUBLE";
    case GL_INT:
        return "GL_INT";
    case GL_INT_VEC4:
        return "GL_INT_VEC4";
    case GL_INT_VEC3:
        return "GL_INT_VEC3";
    case GL_INT_VEC2:
        return "GL_INT_VEC2";
    case GL_UNSIGNED_INT:
        return "GL_UNSIGNED_INT";
    case GL_BYTE:
        return "GL_BYTE";
    case GL_UNSIGNED_BYTE:
        return "GL_UNSIGNED_BYTE";
    case GL_SHORT:
        return "GL_SHORT";
    case GL_UNSIGNED_SHORT:
        return "GL_UNSIGNED_SHORT";
    case GL_BOOL:
        return "GL_BOOL";
    case GL_BOOL_VEC4:
        return "GL_BOOL_VEC4";
    case GL_BOOL_VEC3:
        return "GL_BOOL_VEC3";
    case GL_BOOL_VEC2:
        return "GL_BOOL_VEC2";
    case GL_SAMPLER_2D:
        return "SAMPLER_2D";
    case GL_SAMPLER_3D:
        return "SAMPLER_3D";
    default:
        return null;
    }
}

@property GLsizei stride(GLenum enumeration) {
    switch(enumeration) {
    case GL_FLOAT:
        return GLfloat.sizeof;
    case GL_FLOAT_VEC4:
        return GLfloat.sizeof * 4;
    case GL_FLOAT_VEC3:
        return GLfloat.sizeof * 3;
    case GL_FLOAT_VEC2:
        return GLfloat.sizeof * 2;
    case GL_FLOAT_MAT4:
        return GLfloat.sizeof * 4 * 4;
    case GL_FLOAT_MAT3:
        return GLfloat.sizeof * 3 * 3;
    case GL_FLOAT_MAT2:
        return GLfloat.sizeof * 2 * 2;
    case GL_DOUBLE:
        return GLdouble.sizeof;
    case GL_INT:
        return GLint.sizeof;
    case GL_INT_VEC4:
        return GLint.sizeof * 4;
    case GL_INT_VEC3:
        return GLint.sizeof * 3;
    case GL_INT_VEC2:
        return GLint.sizeof * 2;
    case GL_UNSIGNED_INT:
        return GLuint.sizeof;
    case GL_BYTE:
        return GLbyte.sizeof;
    case GL_UNSIGNED_BYTE:
        return GLubyte.sizeof;
    case GL_SHORT:
        return GLshort.sizeof;
    case GL_UNSIGNED_SHORT:
        return GLushort.sizeof;
    case GL_BOOL:
        return GLboolean.sizeof;
    case GL_BOOL_VEC4:
        return GLboolean.sizeof * 4;
    case GL_BOOL_VEC3:
        return GLboolean.sizeof * 3;
    case GL_BOOL_VEC2:
        return GLboolean.sizeof * 2;
    default:
        throw new Exception("Unknown type " ~ to!string(enumeration));
    }
}

class ShaderException : Exception
{
    this(string errorLog)
    {
        super(errorLog);
    }
}
