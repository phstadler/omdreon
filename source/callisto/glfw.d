module callisto.glfw;
@system:

public {
    import gl3n.linalg;
    import derelict.opengl3.gl3;
    import derelict.glfw3.glfw3;
}

private {
    import std.string : toStringz;
    import std.traits : isSomeString;
}

static this()
{
    import callisto.paths;
    import derelict.util.exception;

    ShouldThrow missingSymFunc(string symName) {
        enum requiredSymbols = [
            "glfwInit",
            "glfwTerminate",
            "glfwWindowHint",
            "glfwCreateWindow",
            "glfwSetInputMode",
            "glfwMakeContextCurrent",
            "glfwGetVideoMode",
            "glfwGetPrimaryMonitor",
            "glfwGetMonitors",
            "glfwGetFramebufferSize",
            "glfwWindowShouldClose",
            "glfwSetWindowShouldClose",
            "glfwSet",
            "glfwGetWindowUserPointer",
            "glfwSetWindowUserPointer",
            "glfwGetKey",
            "glfwDestroyWindow",
            "glfwSwapBuffers",
            "glfwPollEvents"
        ];

        return requiredSymbols.canFind(symName)
                    ? ShouldThrow.Yes
                    : ShouldThrow.No;
    }


    DerelictGL3.missingSymbolCallback = &missingSymFunc;
    DerelictGL3.load();

    version(Win32)      DerelictGLFW3.load(libraryPath("glfw3.dll"));
    else version(OSX)   DerelictGLFW3.load(libraryPath("libglfw3.3.1.dylib"));
    else                DerelictGLFW3.load();

    if (!glfwInit())
        throw new Exception("Could not init GLFW");
}

static ~this()
{
    glfwTerminate(); // closes windows and frees glfw resources
}

/++
 + Represents an area to draw on and a window associated with it.
 + Uses a glfw window internally that can be accessed with window.handle
 + The user pointer can be set via the userPointer property and must be of
 + the template parameter type
 +
 + Examples:
 + ------------------------------------
 + Window!Controller window = Window.newFullscreen();
 + window.userPointer = new Controller;
 + window.renderFrame();
 + window.dispose();
 + ------------------------------------
 +/
struct Window(UserPtrType)
{
    enum defaultSize = vec2i(1024,768);
    enum defaultWindowHints = [
        GLFW_SAMPLES: 4,
        GLFW_REFRESH_RATE: 0,
        GLFW_CONTEXT_VERSION_MAJOR: 3,
        GLFW_CONTEXT_VERSION_MINOR: 3,
        GLFW_OPENGL_PROFILE: GLFW_OPENGL_CORE_PROFILE,
        GLFW_OPENGL_FORWARD_COMPAT: GL_TRUE
    ];

    private GLFWwindow* window;

    /*static Window opCall(GLFWwindow* window)
    {
        return Window(window);
    }*/

    /+
     + Returns a framed window with the specified size in pixels.
     + The context of the window is automatically made current.
     +/
    static Window newWindow(vec2i size=defaultSize, string title="Window", int[int] windowHints=defaultWindowHints)
    {
        foreach(hint, val; windowHints) {
            glfwWindowHint(hint, val);
        }

        GLFWwindow* window = glfwCreateWindow(size.x, size.y, toStringz(title), null, null);
        if (!window)
        {
            throw new Exception("Could not create window");
        }

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

        glfwMakeContextCurrent(window);
        DerelictGL3.reload(); // modern OpenGL functions are accessible when reloaded after context was created

        GLuint vertexArrayID;
        glGenVertexArrays(1, &vertexArrayID);
        glBindVertexArray(vertexArrayID);

        return Window(window);
    }

    /+
     + Returns a fullscreen window with native resolution.
     + The context of the fullscreen window is automatically
     + made current.
     +/
    static Window newFullscreen(int[int] windowHints=defaultWindowHints)
    {
        const(GLFWvidmode)* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        int screenWidth = (*mode).width;
        int screenHeight = (*mode).height;

        int monitorCount;
        auto lastMonitor = glfwGetMonitors(&monitorCount)[monitorCount-1];

        foreach(hint, val; windowHints) {
            glfwWindowHint(hint, val);
        }

        GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, toStringz(""), lastMonitor, null);
        if (!window)
        {
            throw new Exception("Could not create window");
        }

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

        glfwMakeContextCurrent(window);
        DerelictGL3.reload(); // modern OpenGL functions are accessible when reloaded after context was created

        GLuint vertexArrayID;
        glGenVertexArrays(1, &vertexArrayID);
        glBindVertexArray(vertexArrayID);

        return Window(window);
    }

    @property GLFWwindow* handle() { return window; }

    @property vec2i framebufferSize() {
        int x, y;
        glfwGetFramebufferSize(window, &x, &y);
        return vec2i(x, y);
    }

    @property bool shouldClose() { return cast(bool) glfwWindowShouldClose(window); }

    @property void shouldClose(bool should) {
        glfwSetWindowShouldClose(window, should ? 1 : 0);
    }


    private @property void setCallback(string propertyName, T)(T value)
    {
        mixin("glfwSet" ~ propertyName ~ "Callback(window, value);");
    }
    alias setCallback!("Key", GLFWkeyfun) keyCallback;
    alias setCallback!("CursorPos", GLFWcursorposfun) mouseMoveCallback;
    alias setCallback!("MouseButton", GLFWmousebuttonfun) mouseButtonCallback;
    alias setCallback!("WindowFocus", GLFWwindowfocusfun) focusCallback;
    alias setCallback!("FramebufferSize", GLFWframebuffersizefun) resizeCallback;
    alias setCallback!("Scroll", GLFWscrollfun) scrollCallback;

    @property UserPtrType userPointer()
    {
        return cast(UserPtrType) glfwGetWindowUserPointer(window);
    }

    @property void userPointer(UserPtrType userPtr)
    {
        glfwSetWindowUserPointer(window, cast(void*) userPtr);
    }

    @property void title(S)(S newTitle) if(isSomeString!S)
    {
        auto newTitleCStr = toStringz(newTitle);
        glfwSetWindowTitle(window, newTitleCStr);
    }

    bool key(int key)
    {
        return glfwGetKey(window, key) == GLFW_PRESS;
    }

    bool keyUp(int key) {
        return glfwGetKey(window, key) == GLFW_RELEASE;
    }

    void dispose()
    {
        glfwDestroyWindow(window);
    }

    /++ Swaps buffers and polls events +/
    void endFrame() {
        swapBuffers();
        pollEvents();
    }

    void swapBuffers()
    {
        glfwSwapBuffers(window);
    }

    void pollEvents()
    {
        glfwPollEvents();
    }

    GLFWwindow* opCast(T)() if(is(T == GLFWwindow*)) {
        return window;
    }
}
