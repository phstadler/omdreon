module callisto.buffer;

public {
    import derelict.opengl3.gl3;
    import callisto.shader;
}

private {
    import callisto.error;
    import std.string : format;
}

struct Buffer {
    /++
     + Creates a new buffer with the given vertices.
     + The newly created buffer is bound upon return.
     +
     + Side effects:
     +     Binds the created buffer (glBindBuffer)
     +/
    static Buffer create(const GLfloat[] vertices, GLenum target=GL_ARRAY_BUFFER, GLenum usage=GL_STATIC_DRAW)
    out
    {
        checkErrorsOpenGL();
    }
    body
    {
        Buffer buf;
        glGenBuffers(1, &buf.name);
        glBindBuffer(target, buf.name);
        checkErrorsOpenGL();
        glBufferData(target, vertices.length * GLfloat.sizeof, vertices.ptr, usage);
        checkErrorsOpenGL();
        return buf;
    }

    static Buffer create(const GLushort[] indices, GLenum target=GL_ELEMENT_ARRAY_BUFFER, GLenum usage=GL_STATIC_DRAW)
    out
    {
        checkErrorsOpenGL();
    }
    body
    {
        Buffer buf;
        glGenBuffers(1, &buf.name);
        glBindBuffer(target, buf.name);
        glBufferData(target, indices.length * GLushort.sizeof, indices.ptr, usage);
        return buf;
    }

    static Buffer empty()
    {
        Buffer buf;
        glGenBuffers(1, &buf.name);
        return buf;
    }

    GLuint name;

    @property bool valid() { return name != 0; }

    void dispose()
    {
        glDeleteBuffers(1, &name);
    }

    void bind(GLenum target)
    out
    {
        checkErrorsOpenGL(
          "Error binding buffer in",
          [
              GL_INVALID_ENUM: format("The given target %s is not a valid buffer type", target)
          ]
        );
    }
    body
    {
        glBindBuffer(target, name);
    }

    void replaceContents(const GLfloat[] vertices, GLenum target=GL_ARRAY_BUFFER, GLenum usage=GL_STATIC_DRAW)
    {
        glBufferData(target, vertices.length * GLfloat.sizeof, vertices.ptr, usage);
    }

    /++
     + Binds all of the attributes of the given shader.
     + The buffer is assumed to store the data interleaved
     + and in the order of the attribute statements in the shader
     +/
    void use()
    {
        use(Shader.current.attributes.attributes);
    }

    /++
     + Binds the given attributes
     +/
    void use(Attribute[] attributes...)
    out
    {
        checkErrorsOpenGL(
          "Error setting vertex attrib pointer in",
          [
              GL_INVALID_VALUE: "Either index is greater than or equal to GL_MAX_VERTEX_ATTRIBS or\n" ~
                                "size is not 1, 2, 3, 4 or (for glVertexAttribPointer), GL_BGRA or\n" ~
                                "stride is negative",
              GL_INVALID_ENUM: "Type is not an accepted value",
              GL_INVALID_OPERATION: "Either zero is bound to the GL_ARRAY_BUFFER buffer object binding " ~
                                    "point and the pointer argument is not NULL or\n" ~
                                    "size is GL_BGRA and normalized is GL_FALSE or\n" ~
                                    "type is GL_UNSIGNED_INT_10F_11F_11F_REV and size is not 3 or\n" ~
                                    "type is GL_INT_2_10_10_10_REV or GL_UNSIGNED_INT_2_10_10_10_REV and " ~
                                    "size is not 4 or GL_BGRA or\n" ~
                                    "size is GL_BGRA and type is not GL_UNSIGNED_BYTE, GL_INT_2_10_10_10_REV " ~
                                    "or GL_UNSIGNED_INT_2_10_10_10_REV"
          ]
        );
    }
    body
    {
        glBindBuffer(GL_ARRAY_BUFFER, name);
        checkErrorsOpenGL();

        GLsizei stride;
        int offset;
        foreach(a; attributes) {
            stride += a.stride;
        }

        foreach(a; attributes) {
            // attributes with an invalid location are most likely
            // undefined as attributes in the shader.
            // Silently ignore these attributes, so they can
            // safely be left out in the shaders
            if(a.location != -1)
            {
                glEnableVertexAttribArray(a.location);
                checkErrorsOpenGL();

                glVertexAttribPointer(
                    a.location,
                    a.size,
                    a.type,
                    GL_FALSE,
                    stride,
                    cast(GLvoid*) offset
                );
                checkErrorsOpenGL();
            }

            offset += a.stride;
        }
    }

    void render(GLenum mode, GLint first, GLsizei count) const
    in
    {
        GLint boundBuffer;
        glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &boundBuffer);
        assert(boundBuffer == name, "Tried to render buffer, but buffer is not in use");
    }
    out
    {
        checkErrorsOpenGL(
          "Error drawing buffer in",
          [
            GL_INVALID_VALUE: "count is negative",
            GL_INVALID_ENUM: "mode is not an accepted value",
            GL_INVALID_OPERATION: "Either a non-zero buffer object name is bound to an enabled array and" ~
                                  "the buffer object's data store is currently mapped or\n" ~
                                  "a geometry shader is active and mode is incompatible with the input " ~
                                  "primitive type of the geometry shader in the currently installed program object"
          ]
        );
    }
    body
    {
      glDrawArrays(mode, first, count);
    }
}
