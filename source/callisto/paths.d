module callisto.paths;

import std.file;
import std.path;
import std.string;
import std.algorithm : canFind;

/++
 + Absolute path to the project base directory with trailing path separator.
 + Contains the executable or app bundle.
 +/
immutable string projectBaseDirectory;
/++
 + Absolute path to the directory for dynamic libraries.
 + Contains a trailing path separator.
 +/
immutable string libraryDirectory;
/++
 + Absolute path to the application resources directory with trailing path separator.
 +
 + If compiled for mac and RESOURCES_DISABLE_DID_CHANGE is specified as a version,
 + this points to inside the .app bundle.
 +
 + If compiled for windows and RESOURCES_DISABLE_DID_CHANGE is specified as a version,
 + this is set to null.
 +/
immutable string resourcesDirectory;

static this()
{
    version(RESOURCES_DISABLE_DID_CHANGE)
    {
        version(Win32)
        {
            projectBaseDirectory = stripDirectories(thisExePath(), 1) ~ dirSeparator;
            libraryDirectory     = projectBaseDirectory ~ "win32\\";
            resourcesDirectory   = projectBaseDirectory ~ "resources\\";
        }
        else version(OSX)
        {
            projectBaseDirectory = stripDirectories(thisExePath(), 2) ~ dirSeparator;
            libraryDirectory     = projectBaseDirectory ~ "Frameworks/";
            resourcesDirectory   = projectBaseDirectory ~ "Resources/";
        }
    }
    else
    {
        version(Win32)
        {
            projectBaseDirectory = stripDirectories(thisExePath(), 2) ~ dirSeparator;
            libraryDirectory     = projectBaseDirectory ~ "libraries\\win32\\";
            resourcesDirectory   = projectBaseDirectory ~ "resources\\";
        }
        else version(OSX)
        {
            projectBaseDirectory = stripDirectories(thisExePath(), 6) ~ dirSeparator;
            libraryDirectory     = projectBaseDirectory ~ "libraries/mac/";
            resourcesDirectory   = projectBaseDirectory ~ "resources/";
        }
    }

    chdir(projectBaseDirectory);
}

string resourcePath(string relativePath)
in
{
    assert(!canFind(relativePath, ".."),
           format("The resource path cannot access files in parent directories, " ~
                  "remove the \"..\" in \"%s\"", relativePath));

    string path = resourcesDirectory ~ relativePath;
    assert(exists(path),
           format("Resource \"%s\" was not found in the resources directory", path));
}
body
{
    return resourcesDirectory ~ relativePath;
}

string libraryPath(string relativePath)
in
{
    assert(!canFind(relativePath, ".."),
           format("The library path cannot access files in parent directories, " ~
                  "remove the \"..\" in \"%s\"", relativePath));

    string path = libraryDirectory ~ relativePath;
    assert(exists(path),
           format("The dynamic library \"%s\" was not found in the libraries directory", path));
}
body
{
    return libraryDirectory ~ relativePath;
}

/**
 * Strips directories from the given path, exposing the parent
 * directories.
 *
 * Params:
 *       path = The path to strip directories from
 *       count = Strip this many directories
 */
private string stripDirectories(string path, int count)
{
    path = buildNormalizedPath(path); // Use platform dependent seperator
    string seperator = dirSeparator;

    for(; count > 0; --count)
    {
        ptrdiff_t lastSeperator = path.lastIndexOf(seperator);
        if(lastSeperator != -1)
        {
            path = path[0..lastSeperator];
        }
        else
        {
            break;
        }
    }
    return path;
}
