/++
 + Provides classes for perspective and orthogonal cameras, capable of calculating
 + view and projection matrices.
 +
 + Cameras can be used in combination with a Renderer.
 +/
module callisto.camera;

public
{
    import gl3n.linalg;
}

private
{
    import callisto.actor;
}

/++
 + Abstract camera class calculating a view matrix.
 + Projection matrices are calculated in the child classes
 + OrthographicCam and PerspectiveCam
 +/
abstract class Camera
{
    mixin Transform!(Yes.hasPosition,
                     No.hasScale,
                     Yes.hasOrientation,
                     No.hasParent,
                     No.hasPostTransform,
                     recalculateView) T;

    this(vec3 position=vec3(0,0,5), quat orientation=quat.identity, float nearPlane=1, float farPlane=100)
    {
        position_ = position;
        orientation_ = orientation;
        nearPlane_ = nearPlane;
        farPlane_ = farPlane;
        recalculateView();
    }

    @property final void nearPlane(float nearPlane)
    {
        nearPlane_ = nearPlane;
        recalculateProjection();
    }

    @property final void farPlane(float farPlane)
    {
        farPlane_ = farPlane;
        recalculateProjection();
    }

    @property final float nearPlane()
    {
        return nearPlane_;
    }

    @property final float farPlane()
    {
        return farPlane_;
    }

    final void set(vec3 position, vec3 forward, vec3 up)
    {
        position_ = position;
        setOrientation(forward, up);
        recalculateView();
    }

    @property final mat4 view()
    {
        return view_;
    }

    @property final mat4 projection()
    {
        return projection_;
    }

    abstract void resize(vec2i size);

    @property final mat4 viewProjection()
    {
        return view_ * projection_;
    }

protected:
    abstract mat4 calculateProjection();

    void recalculateProjection()
    {
        projection_ = calculateProjection();
    }

private:
    public void recalculateView()
    {
        view_ = mat4.look_at(position_, position_+forward, up);
    }

    mat4 view_;
    mat4 projection_;
    float nearPlane_;
    float farPlane_;
}

/++
 + A camera with perspective projection.
 +/
class PerspectiveCam : Camera
{
    this(vec2i size, float fieldOfView=50.0f)
    {
        size_ = size;
        fieldOfView_ = fieldOfView;
        recalculateProjection();
    }

    override mat4 calculateProjection()
    {
        return mat4.perspective(size_.x, size_.y, fieldOfView_, nearPlane, farPlane);
    }

    override void resize(vec2i size)
    {
        size_ = size;
        recalculateProjection();
    }

    @property float fieldOfView() { return fieldOfView_; }

    @property void fieldOfView(float fieldOfView)
    {
        fieldOfView_ = fieldOfView;
        recalculateProjection();
    }

private:
    vec2i size_;
    float fieldOfView_;
}

/++
 + A camera with orthographic projection.
 + The view height is always 2.0, width
 + depends on aspect ratio.
 +/
class OrthographicCam : Camera
{
    this(vec2i viewportSize, float size=1)
    {
        size_ = size;
        resize(viewportSize);
    }

    override mat4 calculateProjection()
    {
        float top = size_;
        float bottom = -size_;
        float right = size_ * ratio;
        float left = -right;
        return mat4.orthographic(left, right, bottom, top, nearPlane, farPlane);
    }

    override void resize(vec2i viewportSize)
    {
        ratio = cast(float)viewportSize.x / cast(float)viewportSize.y;
        recalculateProjection();
    }

    @property void size(float size)
    {
        size_ = size;
        recalculateProjection();
    }

    @property float size() { return size_; }

private:
    float ratio;
    float size_;
}
