module callisto.navigation;

public
{
    import callisto.shell;
    import callisto.camera;
}

private
{
    import std.math : abs;
    import gl3n.linalg;
    import gl3n.interpolate;
    import std.stdio;
}

class OrbitalNavigation
{
    this(Shell shell, Camera camera, vec3 center=vec3(0,0,0), float distance=10.0f, float rotationSpeed=0.9f, float zoomSpeed=40f)
    {
        camera_ = camera;
        center_ = center;
        shell.mouseMoveEventQueue ~= &move;
        shell.mouseButtonEventQueue ~= &click;
        shell.scrollEventQueue ~= &scroll;
        rotationSpeed_ = rotationSpeed * shell.timestep;
        zoomSpeed_ = zoomSpeed * shell.timestep;
        distance_ = distance;

        updateCamPosition();
    }

    void move(MouseMoveEvent event)
    {
        if(turning && lastMousePosValid_) {
            // Invert controls, feels more natural
            vec2 delta = -(event.position - lastMousePos_) * rotationSpeed_;

            if(abs(delta.x) > 0)
            {
                camera_.orientation = (quat.axis_rotation(delta.x, camera_.up) * camera_.orientation).normalized;
            }

            if(abs(delta.y) > 0)
            {
                vec3 cameraXAxis = cross(camera_.forward, camera_.up).normalized;
                camera_.orientation = (quat.axis_rotation(delta.y, cameraXAxis) * camera_.orientation).normalized;
            }

            updateCamPosition();
        }
        lastMousePos_ = event.position;
        lastMousePosValid_ = true;
    }

    void click(MouseButtonEvent event)
    {
        turning = event.press;
    }

    void scroll(ScrollEvent event)
    {
        distance_ += event.y * zoomSpeed_;
        updateCamPosition();
    }

    @property vec3 center() { return center_; }
    @property void center(vec3 center)
    {
        center_ = center;
        updateCamPosition();
    }

private:
    void updateCamPosition() { camera_.position = center_ - camera_.forward*distance_; }

    vec2 lastMousePos_ = vec2(float.min_normal, float.min_normal);
    bool lastMousePosValid_ = false;
    Camera camera_;
    vec3 center_;
    float distance_;
    float rotationSpeed_;
    float zoomSpeed_;
    bool turning = false;
}
