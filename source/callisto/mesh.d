module callisto.mesh;

public {
    import callisto.shader;
    import callisto.buffer;
}

private {
    import callisto.error;
    import derelict.opengl3.gl3;
    import std.string : format;
    import std.stdio;
}

struct MeshAttr
{
    string name;
    GLint size = -1;
    @property bool valid() { return name != null && name.length > 0 && size != -1; }
}

/*class MeshBuilder
{
    this(GLenum mode=GL_TRIANGLES, MeshAttr[]... attributes)
    {
        mode_ = mode;
        attributes_ = attributes;
    }

    Mesh createMesh()
    {
        return new Mesh(vertices_, indices_, mode_, attributes_);
    }

private:
    GLenum mode_;
    MeshAttr[] attributes_;
    GLfloat[] vertices_;
    GLushort[] indices_;
}*/

class Mesh
{
private:
    GLenum mode;
    Buffer vertexBuffer;
    Buffer indexBuffer;
    GLint attributesShader = 0;
    Attribute[] attributes;
    GLsizei count;

public:
    this(const GLfloat[] vertices, const GLushort[] indices, GLenum mode, MeshAttr[] meshAttributes...)
    out
    {
        size_t floatStride = attributes.stride / GLfloat.sizeof;
        size_t vertexCount = vertices.length;
        assert((vertexCount % floatStride) == 0,
               format("Vertex count was %s but should be a multiple of %s", vertexCount, floatStride));
        assert(vertices != null && vertices.length > 0, "No vertices supplied");
        // TODO check if all attributes have valid types and sizes
    }
    body
    {
        vertexBuffer = Buffer.create(vertices);
        this.mode = mode;
        size_t stride = 0;
        foreach(meshAttr; meshAttributes)
        {
            attributes ~= Attribute(meshAttr.name, -1, GL_FLOAT, meshAttr.size);
            stride += meshAttr.size;
        }

        if(indices != null)
        {
            indexBuffer = Buffer.create(indices);
            this.count = cast(GLsizei) indices.length;
        }
        else
        {
            this.count = cast(GLsizei) (vertices.length / stride);
        }
    }

    void use(Shader shader)
    in
    {
        assert(shader.inUse, "When using a mesh with a shader, it has to be in use");
    }
    out
    {
        checkErrorsOpenGL();
        // TODO are of type GL_FLOAT and if the attribute sizes match
        foreach(a; attributes)
        {
            //assert(a.valid, format("Attribute \"%s\" is not valid. Does the shader have an attribute with that name?", a.name));
        }
    }
    body
    {
        if(shader.name != attributesShader)
        {
            foreach(ref a; attributes)
            {
                a.location = shader.attributeLocation(a.name);
            }
            attributesShader = shader.name;
        }

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer.valid ? indexBuffer.name : 0);
        vertexBuffer.use(attributes);
    }

    void render()
    {
        if(indexBuffer.valid) {
            glDrawElements(mode, count, GL_UNSIGNED_SHORT, null);
        } else {
            glDrawArrays(mode, 0, count);
        }
    }
}
