/++
 + Provides handling of application resources.
 +
 + In Debug builds, resources are read from the Resources directory
 + and can be automatically reloaded when changed on disk.
 +
 + In Release builds, the resources are packaged with the application.
 + This is implemented with compiled resources on Windows and packaging
 + with the .app bundle on OSX
 +/

module callisto.source;
@trusted:

version(RESOURCES_DISABLE_DID_CHANGE) {} else { version = RESOURCES_ENABLE_DID_CHANGE; }

private {
	import std.file;
	import std.datetime;
	import callisto.paths;
}

/++
 + Specifies a handle to a source for data, e.g. shaders or textures.
 +
 + The source object merely holds information to retrieve the data,
 + not the data itself.
 +
 + Examples:
 + ---------------------------
 + // Resources are bundled with the application and are required for
 + // the application to run properly
 + TextSource res1 = textResource("shader.vert.glsl");
 + // Files can be anywhere on the file system
 + DataSource res2 = dataFile("main.db");
 +
 + string shaderSource = res1.read(); // actual data
 + ---------------------------
 +/
interface Source(T)
{
	T read();
	/++ Returns true if the source has been modified since the last read +/
	@property bool didChange();
}

/++
 + Specifies a source for a string, encoded with UTF-8 without BOM.
 +
 + The source object merely holds information to retrieve the string,
 + not the data itself.
 +
 + Examples:
 + ---------------------------
 + TextSource text = textResource("shaders/passthru.vert.glsl");
 + ---------------------------
 +/
alias Source!string TextSource;
/++
 + Specifies a source for binary data.
 +
 + The source object merely holds information to retrieve the data,
 + not the data itself.
 +
 + Examples:
 + ---------------------------
 + DataSource data = dataResource("textures/grass.bmp");
 + ---------------------------
 +/
alias Source!(void[]) DataSource;

/++
 + Gets a TextSource representing the file specified with path.
 + Can be absolute or relative to working directory (defaults to projectBaseDirectory)
 +/
TextSource textFile(string path)
{
	return new TextFile(path);
}

/++
 + Gets a DataSource representing the file specified with path.
 + Can be absolute or relative to working directory (defaults to projectBaseDirectory)
 +/
DataSource dataFile(string path)
{
	return new DataFile(path);
}

/++
 + Gets a TextSource for a resource specified with its path relative
 + to the resources directory.
 +
 + With RESOURCES_DISABLE_DID_CHANGE enabled on windows, the returned source
 + will represent a compiled resource rather than a file on the filesystem.
 +/
TextSource textResource(string path)
{
	// is defined if Windows and version RESOURCES_DISABLE_DID_CHANGE
	static if(is(TextWin32Resource))
	{
		return new TextWin32Resource();
	}
	else
	{
		return new TextFile(resourcePath(path));
	}
}

/++
 + Gets a DataSource for a resource specified with its path relative
 + to the resources directory.
 +
 + With RESOURCES_DISABLE_DID_CHANGE enabled on windows, the returned source
 + will represent a compiled resource rather than a file on the filesystem.
 +/
DataSource dataResource(string path)
{
	static if(is(DataWin32Resource))
	{
		return new DataWin32Resource();
	}
	else
	{
		return new DataFile(resourcePath(path));
	}
}

private:


abstract class AbstractFile(T) : Source!T {
	string path_;

	version(RESOURCES_DISABLE_DID_CHANGE)
	{
		/* On release builds, don't check for changed assets */

		this(string path)
		{
			path_ = path;
		}

		T read()
		{
			return doRead();
		}

		protected @property bool didChange() { return false; }
		protected @property void didChange(bool didChange) {}
	}
	else
	{
		/* On debug builds, check if assets have changed (e.g. recompile changed shaders) */

		/++ minimum time in seconds between to modified checks reaching out to the file system +/
		enum checkModifiedInterval = 0.1f;
		StopWatch watch;
		SysTime lastRead_ = SysTime.min;
		SysTime lastModified_;

		this(string path)
		{
			path_ = path;
			lastModified_ = timeLastModified(path);
			watch.start();
		}

		T read()
		{
			T read = doRead();
			lastRead_ = Clock.currTime;
			return read;
		}

		void updateLastModified()
		{
			if(watch.peek().to!("seconds", float) > checkModifiedInterval)
			{
				watch.reset();
				lastModified_ = timeLastModified(path_);
			}
		}

		@property bool didChange()
		{
			updateLastModified();
			return lastModified_ > lastRead_;
		}
	}

	abstract T doRead();
}

class TextFile : AbstractFile!string
{
	this(string path)
	{
		super(path);
	}

	override string doRead() {
		return std.file.readText(path_);
	}
}

class DataFile : AbstractFile!(void[])
{
	this(string path)
	{
		super(path);
	}

	override void[] doRead()
	{
		return std.file.read(path_);
	}
}


/*version(Windows)
version(RESOURCES_DISABLE_DID_CHANGE)
{
	abstract class AbstractWin32Resource(T) : Source!T
	{
		final void[] loadWin32Resource()
		{
			return null; // TODO Load with LoadResource win32 function
		}
	}

	class TextWin32Resource : AbstractWin32Resource!string
	{
		string read()
		{
			string data = cast(string) loadWin32Resource();
			std.utf.validate(data);
			return data;
		}
	}

	class DataWin32Resource : AbstractWin32Resource!(void[])
	{
		alias read = loadWin32Resource;
	}
}*/
